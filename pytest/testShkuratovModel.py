import math
import os.path
import numpy as np
import json
import kernelo as ker


print(os.getcwd())
with open('pytest/new_test_shkuratov.json', 'r') as f:
    data = json.load(f)

D = 50
row_size = 3
col_size = D

scalingCoeffs = [1.0,1.5,1.5,1.5,1.5]
offset = [0,0,0.2,0,0]


# # Create JSON file with geometries
geometries = np.empty((row_size,col_size))
# var_geom = ["inc", "eme", "phi"]
# for j in range(3):
#     i=0
#     for v in data[var_geom[j]]:
#         geometries[j,i] = v
#         i+=1

# geom = {'geometries': [[geometries[j,:].tolist()] for j in range(3)]
# }
# with open('geometries_shkuratov.json', 'w') as fp:
#     json.dump(geom, fp)

## INTEGRATION au code C++
physicalModel = ker.ExternalModelConfig("ShkuratovModel5p", "ShkuratovModel5pPython", "pytest/models").create()



### TEST
N = 10000
L = 5
variables = ["an", "mu1", "nu", "m", "mu2"]
photometries = np.empty((L,N))

# Read photometries
for l in range(L):
    n=0
    for v in data[variables[l]]:
        photometries[l,n] = (float(v) - offset[l]) / scalingCoeffs[l]
        n+=1


# Read expected results
expected_results = np.empty((D,N))
n=0
for v in data["y"]:
    l=0
    for w in v:
        expected_results[l,n] = float(w)
        l+=1
    n+=1


# compute results from the model
result = np.empty((D,))
assert_list = []
for n in range(N):
    result = physicalModel.F(photometries[:,n])
    assert_list.append(np.allclose(expected_results[:,n], result, rtol=1e-8))


# print(assert_list)
print(False in assert_list)
print(True in assert_list)

print(expected_results[:10,n])
print(result[:10])