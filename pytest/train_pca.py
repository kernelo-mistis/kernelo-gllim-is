# %%
import numpy as np
import mat73 # loading matlab 7.3 files
import os.path
import pickle
import time
import logging
import matplotlib.pyplot as plt
import random
logging.getLogger().setLevel(logging.INFO)
import kernelo as ker

# %% [markdown]
# ### Enter Dico and gllim name 

# %%
dico_name = "DICO_PCA_10"
gllim_name = "DICO_PCA_10_n_50000_K_10_max_iter_100_full_nb_exp_8"
pca_components_name = dico_name + '_components'

# %% [markdown]
# ### Load Dico

# %%
print("aaa")
print(os.getcwd())
path_to_dico = 'dico/' + dico_name
with open(path_to_dico, "rb") as f:
    dico = pickle.load(f)

path_to_components = 'dico/' + pca_components_name
with open(path_to_components, "rb") as f:
    pca_components = pickle.load(f)

MRSignals = dico['MRSignals']
Parameters = dico['Parameters']
n_signals = MRSignals.shape[0]
n_pulses = MRSignals.shape[1]
n_parameters = Parameters.shape[1]
parameters_label = ['T1', 'T2', 'df', 'B1rel', 'SO2', 'Vf', 'R']

print(('Loading of a dictionary of {} signals of length {} pulses. \nThere are {} parameters : {}.').format(n_signals, n_pulses, n_parameters, parameters_label))

# %% [markdown]
# ### Reduction of samples

# %%
# MRSignals = MRSignals[:50000,:]
# Parameters = Parameters[:50000,:]

# n_signals = MRSignals.shape[0]

# %% [markdown]
# #### Example of Signal

# %%
example_index = random.randint(0, n_signals)
example_index = 60

signal_dico = MRSignals[example_index, :]
signal_pca = np.dot(pca_components.T, signal_dico) 
plt.figure(figsize=(20, 10))
pulses = np.arange(signal_pca.shape[0]) 
plt.plot(pulses, signal_pca, linewidth=3)
# plt.plot(pulses, signal_pca, linewidth=3)
plt.xlabel('Pulses')
plt.ylabel('Signal Value')
plt.title('Example of one signal from the dictionary')
plt.grid(True)
plt.show()

# %% [markdown]
# ### Normalization

# %%
# def logit_affine(u, a, b):
#     return np.log( (u-a) / (b-u) )

# parameters_bounds = {
#     'inf': np.empty(n_parameters),
#     'sup': np.empty(n_parameters)
# }
# alpha = 1e-3 # dilatation coefficient
# for i in range(n_parameters): 
#     beta = (max(Parameters[:, i]) - min(Parameters[:, i])) * alpha # spacing constant
#     parameters_bounds['inf'][i] = min(Parameters[:, i]) - beta
#     parameters_bounds['sup'][i] = max(Parameters[:, i]) + beta
# print("Parameters bounds : {}".format(parameters_bounds))

# # NORMALIZATION and TRANSFORMATION
# MRSignals = MRSignals / np.linalg.norm(MRSignals, ord=2, axis=1, keepdims=True)
# Parameters = logit_affine(Parameters, parameters_bounds['inf'], parameters_bounds['sup'])

# %% [markdown]
# ### Set up GLLiM

# %%
seed = 12345

gllim_conf = {
    "K": 10,
    "gamma_type": "Full",
    "sigma_type": "Full",
    "init_config": {
        "multi": {
            "type": "multi",
            "config": {
                "seed": seed,
                "nb_iter_em": 5,
                "nb_exp": 8,
                "gmm_learning_config": {
                    "kmeans_iteration": 5,
                    "em_iteration": 10,
                    "floor": 1e-8
                }
            }
        },
        "fixed": {
            "type": "fixed",
            "config": {
                "seed": seed,
                "gmm_learning_config": {
                    "kmeans_iteration": 5,
                    "em_iteration": 10,
                    "floor": 1e-8
                }
            }
        },
    },
    "train_config": {
        "em": {
            "type": "em",
            "config": {
                "max_iteration": 100,
                "ratio_ll": 0.00001,
                "floor": 1e-8
            }
        },
        "gmm": {
            "type": "gmm",
            "config": {
                "kmeans_iteration": 10,
                "em_iteration": 10,
                "floor": 1e-8
            }
        }
    }
}

# %%
# GLLiM params
D = n_pulses
L = n_parameters

# EM training method
train_context = gllim_conf["train_config"]["em"]["config"]
learningConfig = ker.EMLearningConfig(train_context["max_iteration"], train_context["ratio_ll"], train_context["floor"])

# # GMM training method
# train_context = gllim_conf["train_config"]["gmm"]["config"]
# learningConfig = ker.GMMLearningConfig(train_context["kmeans_iteration"], train_context["em_iteration"], train_context["floor"])

# MULTI initialisation method
init_context = gllim_conf["init_config"]["multi"]["config"]
gmmInitConfig = ker.GMMLearningConfig(init_context["gmm_learning_config"]["kmeans_iteration"], init_context["gmm_learning_config"]["em_iteration"], init_context["gmm_learning_config"]["floor"])
initConfig = ker.MultInitConfig(init_context["seed"], init_context["nb_iter_em"], init_context["nb_exp"], gmmInitConfig)

# # FIXED initialisation method
# init_context = gllim["init_config"]["fixed"]["config"]
# gmmInitConfig = ker.GMMLearningConfig(init_context["gmm_learning_config"]["kmeans_iteration"], init_context["gmm_learning_config"]["em_iteration"], init_context["gmm_learning_config"]["floor"])
# initConfig = ker.MultInitConfig(init_context["seed"], gmmInitConfig)

gllim = ker.GLLiM(D, L, gllim_conf["K"], gllim_conf["gamma_type"], gllim_conf["sigma_type"], initConfig, learningConfig)

# %% [markdown]
# ### Train GLLiM

# %%
gllim_model_file_name = "gllim/" + gllim_name + ".file"
if not os.path.isfile(gllim_model_file_name):  # This is very useful to not train Gllim model at every simulation

    print("Initializing GLLIM model")
    t0 = time.time()
    gllim.initialize(Parameters, MRSignals)
    tf = time.time() - t0
    print(("Initialisation step took {} min {} sec").format(tf//60, tf%60))

    print("Training model")
    t0 = time.time()
    gllim.train(Parameters, MRSignals)
    tf = time.time() - t0
    print(("Training step took {} min {} sec").format(tf//60, tf%60))

    gllim_parameters = gllim.exportModel() # you can export your gllim model parameters
    with open(gllim_model_file_name, "wb") as f:
        pickle.dump(gllim_parameters, f, pickle.HIGHEST_PROTOCOL)
    f.close()
    print("GLLIM model saved")
else:
    with open(gllim_model_file_name, "rb") as f:
        gllim_parameters = pickle.load(f)
        gllim.importModel(gllim_parameters)
    print("GLLIM model loaded")

# %% [markdown]
# ### Direct Gllim

# %%
# print(gllim_parameters.K)
# print(gllim_parameters.L)
# print(gllim_parameters.D)
# print(gllim_parameters.Pi)
# print(type(gllim_parameters.Pi))
print(gllim_parameters.Pi.shape)
# print(gllim_parameters.B)
# print(type(gllim_parameters.B))
print(gllim_parameters.B.shape)
# print(gllim_parameters.A)
print(gllim_parameters.A.shape)
# print(gllim_parameters.Sigma)
print(gllim_parameters.Sigma.shape)
print(gllim_parameters.C)
print(gllim_parameters.C.shape)
print(gllim_parameters.Gamma)
print(gllim_parameters.Gamma.shape)

print(sum(gllim_parameters.Pi))

# %%
K = gllim_parameters.Pi.shape[0]
L = gllim_parameters.C.shape[0]
D = gllim_parameters.B.shape[0]

def gllim_direct(x):

    weights = np.zeros(K)
    for k in range(K):
        if gllim_parameters.Pi[k] == 0 :
            weights[k] = -np.inf
        else :
            log_det_gamma = np.linalg.slogdet(gllim_parameters.Gamma[k])[1] # log_det de Gamma. slogdet renvoit un tuple avec (signe, log_det) mais dansnotre cas Gamma et def positive donc le signe c'est 1
            x_u = x - gllim_parameters.C[:,k]
            if log_det_gamma != -np.inf:
                quadratic = np.dot( x_u ,np.dot(np.linalg.inv(gllim_parameters.Gamma[k]), x_u) )
                weights[k] = np.log(gllim_parameters.Pi[k]) - 0.5 * (L * np.log(2* np.pi) + log_det_gamma + quadratic)

    # weights normalisation
    result = 0
    max = np.max(weights)
    if max != -np.inf :
        for k in range(K):
            result += np.exp(weights[k] - max)
        result = np.log(result) + max
    if result != -np.inf:
        weights = np.exp(weights - result)

    # Compute mean
    mean = np.zeros(D)
    for k in range(K):
        mean += weights[k] * (np.dot(gllim_parameters.A[k,:,:], x) + gllim_parameters.B[:,k])
    return mean



# %%
signal = gllim_direct(Parameters[500,:])

# %%
n = random.randint(0, n_signals)
signal_dico = MRSignals[n, :]
signal = gllim_direct(Parameters[n,:])

# Reconstruction with PCA components
signal_dico = np.dot(pca_components.T, signal_dico)
signal = np.dot(pca_components.T, signal)

pulses = np.arange(signal_dico.shape[0])

plt.figure(figsize=(20, 10))
plt.plot(pulses, signal_dico, linewidth=3, label="Dico signal")
plt.plot(pulses, signal, label="Glim direct signal")
plt.xlabel('Pulses')
plt.ylabel('Signal Value')
plt.title('One signal comparison from direct gllim model')
plt.legend()
plt.grid(True)
plt.show()

# %%


# %% [markdown]
# ### Test GLLiM

# %%
pred_config = {
    "k_merged": 2,
    "k_pred_means": 2,
    "threshold": 1e-10
}

nb_centers = pred_config["k_merged"]
null_pred_dim = (
    L,
    pred_config["k_pred_means"],
    pred_config["k_merged"]
)

# %%
predicator = ker.PredictionConfig(pred_config["k_merged"], pred_config["k_pred_means"], pred_config["threshold"], gllim).create()
# prediction_means = [[] for i in range(nb_centers+1)] # list[0] is the list for mean ; list[1] is the list for center1 ; list[2] is the list for center2
# prediction_reconstruction_error = [[] for i in range(nb_centers+1)]
# mean_prop_laws = [] # Proposition laws for IS and IMIS
# center_prop_laws = []

# %%
import billiard as mp

def return_null_pred(null_pred_dim):
    # NOTE: faire ça dans le "cinit" de la lib Kernelo, 
    # de sorte de ker.PredictionResultExport(L,k_pred_mean,kmerged) renvoie des matrices de zeros.
    L, k_pred_mean, k_merged = null_pred_dim
    null_pred = ker.PredictionResultExport()
    null_pred.meansPred.mean = np.zeros(L)
    null_pred.meansPred.variance = np.zeros(shape=(L))
    null_pred.meansPred.gmm_weights = np.zeros(shape=(k_pred_mean))
    null_pred.meansPred.gmm_means = np.zeros(shape=(L, k_pred_mean))
    null_pred.meansPred.gmm_covs = np.zeros(shape=(k_pred_mean, L, L))
    null_pred.centersPred.weights = np.zeros(shape=(k_merged))
    null_pred.centersPred.means = np.zeros(shape=(L, k_merged))
    null_pred.centersPred.covs = np.zeros(shape=(k_merged, L, L))
    return null_pred

def predict_mp(predictor, queue: mp.Manager().Queue(), shared_list: mp.Manager().list()):
    while not queue.empty():
        signal, incertitude, j, i, nb_pred, nb_pixel_y, null_pred_dim = queue.get()
        # Check if all instance is (float, int). This check shoulb be done at the pre-processing step
        # Note that NaN is float object.
        if not ( all(isinstance(x, (int,float)) for x in signal) and\
                all(isinstance(x, (int,float)) for x in incertitude) ):
            print(("[Prediction] Pixel ({},{}) contains unwanted elements").format(i,j))
            shared_list.append((i, j, return_null_pred(null_pred_dim)))
        else:
            if np.any(np.isnan(signal)) or np.any(np.isnan(incertitude)):
                print(("[Prediction] Pixel ({},{}) contains nan elements").format(i,j))
                shared_list.append((i, j, return_null_pred(null_pred_dim)))
            else:
                print(("[Prediction] Pixel ({},{})").format(i,j))
                shared_list.append((i, j, predictor.predict(signal, incertitude)))
        
        nb_pred_processed = (j + 1) + (i * nb_pixel_y)
        if nb_pred_processed % (nb_pred // 10) == 0:
            print("[Prediction] Processed : {}/{}".format(nb_pred_processed, nb_pred))

def predict(observations, predictor, null_pred_dim):

    nb_pixel_x = observations.shape[0]
    nb_pixel_y = 1
    nb_pred = nb_pixel_x * nb_pixel_y
    predictions = np.empty((nb_pixel_x, nb_pixel_y), dtype=object)
    incertitudes = np.ones(n_pulses)*1e-5
    print("[Prediction] Starting estimation of {} predictions for image of size ({}x{})".format(nb_pred, nb_pixel_x, nb_pixel_y))
    
    manager = mp.Manager()
    shared_list = manager.list()
    queue = manager.Queue()
    print("[Prediction] Multiprocessing with {} CPU cores available".format(str(len(os.sched_getaffinity(0)))))

    # Create arguments queue for process function
    for i in range(nb_pixel_x):
        for j in range(nb_pixel_y):
            queue.put((observations[i,:], incertitudes, j, i, nb_pred, nb_pixel_y, null_pred_dim))

    # Create Processes
    processes = []
    # for num in range(mp.cpu_count()*2):
    for num in range(len(os.sched_getaffinity(0))*2):
        processes.append(mp.Process(target=predict_mp, args=(predictor, queue, shared_list)))

    for process in processes:
        process.start()

    # Wait for processes to terminate and close them properly
    mp.connection.wait(p.sentinel for p in processes)
    for p in processes:
        p.join()
        p.close()
    for i, j, result in shared_list:
        predictions[i, j] = result
    print("[Prediction] Processed : {}/{}".format(nb_pred, nb_pred))

    return predictions

# %%
n_test = 1000
index = [i for i in range(n_signals)]
index_test = random.sample(index, n_test)
index_test.sort()
MRSignals_test = MRSignals[index_test,:]
Parameters_test = Parameters[index_test,:]

# %%
prediction_file_name = "pred-testing-dataset/" + "Test1000__" + gllim_name
# prediction_file_name = "pred-brain-image/" + "nifti_CS3_slice2_normalized__DICOsmall2_X_logit_Y_NormL2_K50_full_diag.file"
if not os.path.isfile(prediction_file_name):
    predictions = predict(MRSignals_test, predicator, null_pred_dim)
    print("Predictions step is done")
    with open(prediction_file_name, "wb") as f:
        pickle.dump(predictions, f, pickle.HIGHEST_PROTOCOL)
    f.close()
else:
    with open(prediction_file_name, "rb") as f:
        predictions = pickle.load(f)
    print("Predictions loaded")

# %% [markdown]
# ### Prediction Analysis

# %%
def compute_reconstruction_error(reconstruction, observation):
    return np.linalg.norm(observation - reconstruction) / np.linalg.norm(observation)

# def compute_reconstruction_error(reconstruction, observation):
#     err = abs(observation - reconstruction)
#     print(err)
#     print(err.shape)
#     return err

prediction_means = np.array([predictions[i,0].meansPred.mean for i in range(predictions.shape[0])])
relative_error = np.array([compute_reconstruction_error(prediction_means[:,l], Parameters_test[:,l]) for l in range(L)])
# relative_error = np.array([
#     np.mean(
#         [compute_reconstruction_error(prediction_means[i,l], Parameters_test[i,l]) for i in range(500)]
#     ) for l in range(L)])
prediction_centers = []
relative_error_centers = []
for center in range(nb_centers):
    prediction_centers.append(np.array([predictions[i,0].centersPred.means[:, center] for i in range(predictions.shape[0])]))
    relative_error_centers.append(np.array([compute_reconstruction_error(prediction_centers[center][:,l], Parameters_test[:,l]) for l in range(L)]))
    # relative_error_centers = np.array([
    # np.mean(
    #     [compute_reconstruction_error(prediction_centers[center][i,l], Parameters_test[i,l]) for i in range(500)]
    # ) for l in range(L)])

# %%


# %% [markdown]
# ### Plot Parameters estimation

# %%
index_plot = 900

index_params = [0,1,2,3,4,5,6]

signal_dico = Parameters_test[index_plot, index_params]
signal_pred = prediction_means[index_plot,index_params]
signal_center = []
for center in range(nb_centers):
    signal_center.append(prediction_centers[center][index_plot,index_params])
pulses = np.arange(signal_dico.shape[0])

plt.figure(figsize=(20, 10))
plt.plot(index_params, signal_dico, 'x', color='k', mew=3, ms=25, label='True signal', linewidth=1)
plt.plot(index_params, signal_pred, 'o', color='r', mew=3, ms=10, label='Pred by the mean signal')
for center in range(nb_centers):
    plt.plot(index_params, signal_center[center], '+',mew=3, ms=10, label='Pred by the center {} signal'.format(center))
plt.legend()
plt.xlabel('Pulses')
plt.ylabel('Signal Value')
plt.title('Signal Plot')
# x_ticks = np.arange(0, signal_dico.shape[0] + 1, 100)  # Customize tick locations
# y_ticks = np.arange(0, 0.5, 0.5)  # Customize y-axis tick locations
# plt.xticks(x_ticks)
# plt.yticks(y_ticks)
plt.grid(True)
plt.show()

# %% [markdown]
# ### Plot relative error for each parameter

# %%
plt.figure(figsize=(20, 10))

# hist_list = [relative_error] + relative_error_centers
# hist_label = ['Pred by mean'] + ['Pred by center {}'.format(center) for center in range(nb_centers)]
# plt.hist(hist_list, bins=bins, label=hist_label)
plt.plot(index_params, relative_error, '+', color='r',mew=3, ms=10, label='Pred by mean')
for center in range(nb_centers):
    plt.plot(index_params, relative_error_centers[center], label='Pred by center {}'.format(center))
plt.legend()
plt.xlabel('Parameters')
plt.ylabel('Relative error')
plt.title('Relative error')
plt.grid(True)
plt.show()


# %%



