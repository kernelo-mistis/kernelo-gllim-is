import os.path
import math
import numpy as np
import matplotlib.pyplot as plt
import pickle
import logging
logging.getLogger().setLevel(logging.INFO)
import kernelo as ker


def compute_reconstruction_error(reconstruction, observation):
    return np.linalg.norm(observation - reconstruction) / np.linalg.norm(observation)

def compute_error_on_x(pred, obs_1, obs_2):
    err_1 = np.linalg.norm(pred - obs_1, ord=np.inf)
    err_2 = np.linalg.norm(pred - obs_2, ord=np.inf)
    return min(err_1, err_2)

K = 70
gllim_name = "testModel_K" + str(K)
nb_centers = 2
n_test = 5

# For loop in order to run n_test
# average error list [mean list, center_1 list, center-2 list]
prediction_reconstruction_error_avg = [[] for i in range(nb_centers+1)]
is_reconstruction_error_avg = [[] for i in range(nb_centers+1)]
imis_reconstruction_error_avg = [[] for i in range(nb_centers+1)]
prediction_error_on_x_avg = [[] for i in range(nb_centers+1)]
is_error_on_x_avg = [[] for i in range(nb_centers+1)]
imis_error_on_x_avg = [[] for i in range(nb_centers+1)]
# standard deviation list [mean list, center_1 list, center-2 list]
prediction_reconstruction_error_std = [[] for i in range(nb_centers+1)]
is_reconstruction_error_std = [[] for i in range(nb_centers+1)]
imis_reconstruction_error_std = [[] for i in range(nb_centers+1)]
prediction_error_on_x_std = [[] for i in range(nb_centers+1)]
is_error_on_x_std = [[] for i in range(nb_centers+1)]
imis_error_on_x_std = [[] for i in range(nb_centers+1)]
for n in range (n_test):
    print("     * Experience {}".format(n))

    # Create "physical" model
    dir_path = os.path.dirname(os.path.realpath(__file__))
    test_model = ker.TestModelConfig().create()

    # Create StatModel
    sigma = 0.01
    variances = np.ones(test_model.get_D_dimension()) * sigma
    stat_model = ker.GaussianStatModelConfig("sobol", test_model, variances, 12345).create()

    # Create GLLIM model, including its initialization and training configuration
    learningConfig = ker.EMLearningConfig(200, 1e-5, 1e-12)
    initConfig = ker.MultInitConfig(seed=12345, nb_iter_EM=10, nb_experiences=10, gmmLearningConfig=ker.GMMLearningConfig(5, 10, 1e-12))
    gllim = ker.GLLiM(test_model.get_D_dimension(), test_model.get_L_dimension(), K, "Full", "Diag", initConfig, learningConfig)

    gllim_model_file_name = dir_path + "/pytest/savedFiles/" + gllim_name
    if not os.path.isfile(gllim_model_file_name):  # This is very useful to not train Gllim model at every simulation

        # Initialize and train GLLIM model
        print("Generating dataset")
        x_gen, y_gen = stat_model.gen_data(50000)
        print("Initializing GLLIM model")
        gllim.initialize(x_gen, y_gen)
        print("Training model")
        gllim.train(x_gen, y_gen)

        gllim_parameters = gllim.exportModel() # you can export your gllim model parameters
        with open(gllim_model_file_name, "wb") as f:
            pickle.dump(gllim_parameters, f, pickle.HIGHEST_PROTOCOL)
        f.close()
        print("GLLIM model saved")
    else:
        with open(gllim_model_file_name, "rb") as f:
            gllim_parameters = pickle.load(f)
            gllim.importModel(gllim_parameters)
        print("GLLIM model loaded")


    ### Load your data ###
    print("Generating observations for example")
    n_observations = 500
    alpha_obs = 1000.
    x_obs = np.zeros((n_observations, test_model.get_L_dimension()))
    x_obs_2 = np.zeros((n_observations, test_model.get_L_dimension())) # second solution for parameter X2
    y_obs = np.zeros((n_observations, test_model.get_D_dimension()))
    y_obs_noised = np.zeros((n_observations, test_model.get_D_dimension()))
    y_obs_noise = np.zeros((n_observations, test_model.get_D_dimension()))

    for i in range(n_observations):
        for j in range(test_model.get_L_dimension()):
            x_obs[i, j] = 0.4 * math.sin(2.*math.pi*i/n_observations + (j * math.pi/4.)) + 0.5
            x_obs_2[i, j] = x_obs[i, j]
        x_obs_2[i, 2] = 0.4 * math.sin(2.*math.pi*i/n_observations + (2 * math.pi/4.) + np.pi) + 0.5
    for i in range(n_observations):
        y_obs[i] = test_model.F(x_obs[i])
        # Add noise for each Y component
        for j in range(test_model.get_D_dimension()):
            y_obs_noise[i,j] = np.random.normal(0, pow(y_obs[i][j]/alpha_obs, 2))
    y_obs_noised = y_obs + y_obs_noise


    ### Predictions ###

    # Gllim
    predicator = ker.PredictionConfig(nb_centers, nb_centers, 1e-10, gllim).create()
    prediction_means = [[] for i in range(nb_centers+1)] # list[0] is the list for mean ; list[1] is the list for center1 ; list[2] is the list for center2
    prediction_reconstruction_error = [[] for i in range(nb_centers+1)]
    prediction_error_on_x = [[] for i in range(nb_centers+1)]
    prop_laws = [[] for i in range(nb_centers+1)] # Proposition laws for IS and IMIS
    print("Computing predictions")
    for i in range(n_observations):
        prediction = predicator.predict(y_obs_noised[i], y_obs_noise[i])
        x_pred = prediction.meansPred.mean
        y_pred = test_model.F(x_pred)
        prediction_means[0].append(x_pred)
        prediction_reconstruction_error[0].append(compute_reconstruction_error(y_pred, y_obs_noised[i]))
        prediction_error_on_x[0].append(compute_error_on_x(x_pred, x_obs[i], x_obs_2[i]))
        mean_prop_law = ker.GaussianMixturePropositionConfig( # Proposition law for IS and IMIS
            prediction.meansPred.gmm_weights, 
            prediction.meansPred.gmm_means,
            prediction.meansPred.gmm_covs).create()
        prop_laws[0].append(mean_prop_law)

        for center in range(1, nb_centers+1):
            x_pred = prediction.centersPred.means[:, center-1]
            y_pred = test_model.F(x_pred)
            prediction_means[center].append(x_pred)
            prediction_reconstruction_error[center].append(compute_reconstruction_error(y_pred, y_obs_noised[i]))
            prediction_error_on_x[center].append(compute_error_on_x(x_pred, x_obs[i], x_obs_2[i]))
            center_prop_law = ker.GaussianRegularizedPropositionConfig(
                prediction.centersPred.means[:, center-1],
                prediction.centersPred.covs[center-1, :, :]).create()
            prop_laws[center].append(center_prop_law)

    # Gllim-IS
    sampler_is = ker.ImportanceSamplingConfig(10000, stat_model).create()
    is_means = [[] for i in range(nb_centers+1)]
    is_reconstruction_error = [[] for i in range(nb_centers+1)]
    is_error_on_x = [[] for i in range(nb_centers+1)]
    print("Computing IS")
    for i in range(n_observations):
        result = sampler_is.execute(prop_laws[0][i], y_obs_noised[i], y_obs_noise[i])
        x_pred = result.mean
        y_pred = test_model.F(x_pred)
        is_means[0].append(x_pred)
        is_reconstruction_error[0].append(compute_reconstruction_error(y_pred, y_obs_noised[i]))
        is_error_on_x[0].append(compute_error_on_x(x_pred, x_obs[i], x_obs_2[i]))

        for center in range(1, nb_centers+1):
            result = sampler_is.execute(prop_laws[center][i], y_obs_noised[i], y_obs_noise[i])
            x_pred = result.mean
            y_pred = test_model.F(x_pred)
            is_means[center].append(x_pred)
            is_reconstruction_error[center].append(compute_reconstruction_error(y_pred, y_obs_noised[i]))
            is_error_on_x[center].append(compute_error_on_x(x_pred, x_obs[i], x_obs_2[i]))

    # Gllim-IMIS
    sampler_imis = ker.ImisConfig(2000, 10, 500, stat_model).create()
    imis_means = [[] for i in range(nb_centers+1)]
    imis_reconstruction_error = [[] for i in range(nb_centers+1)]
    imis_error_on_x = [[] for i in range(nb_centers+1)]
    print("Computing IMIS")
    for i in range(n_observations):
        result = sampler_imis.execute(prop_laws[0][i], y_obs_noised[i], y_obs_noise[i])
        x_pred = result.mean
        y_pred = test_model.F(x_pred)
        imis_means[0].append(x_pred)
        imis_reconstruction_error[0].append(compute_reconstruction_error(y_pred, y_obs_noised[i]))
        imis_error_on_x[0].append(compute_error_on_x(x_pred, x_obs[i], x_obs_2[i]))

        for center in range(1, nb_centers+1):
            result = sampler_imis.execute(prop_laws[center][i], y_obs_noised[i], y_obs_noise[i])
            x_pred = result.mean
            y_pred = test_model.F(x_pred)
            imis_means[center].append(x_pred)
            imis_reconstruction_error[center].append(compute_reconstruction_error(y_pred, y_obs_noised[i]))
            imis_error_on_x[center].append(compute_error_on_x(x_pred, x_obs[i], x_obs_2[i]))


    # ### Plot results ###
    # print("Plotting results")
    # fig, axs = plt.subplots(2, 2, num="Graphs : Test Model")
    # fig.suptitle("Parameters estimation", fontsize=16)
    # n_observations_list = np.arange(1, n_observations + 1)

    # for l in range(test_model.get_L_dimension()):
    #     i = 0 if (l<2) else 1
    #     j = l if (l<2) else l-2

    #     axs[i,j].plot(n_observations_list, x_obs[:,l], 'k-', label='observation')
    #     axs[i,j].plot(n_observations_list, [x[l] for x in prediction_means[0]], 'b.', label='prediction mean')
    #     axs[i,j].plot(n_observations_list, [x[l] for x in is_means[0]], 'y.', label='is mean')
    #     axs[i,j].plot(n_observations_list, [x[l] for x in imis_means[0]], 'r.', label='imis mean')
    #     # axs[i,j].plot(n_observations_list, [x[l] for x in prediction_means[1]], 'b.', label='prediction center_1')
    #     # axs[i,j].plot(n_observations_list, [x[l] for x in is_means[1]], 'y.', label='is center_1')
    #     # axs[i,j].plot(n_observations_list, [x[l] for x in imis_means[1]], 'r.', label='imis center_1')
    #     # axs[i,j].plot(n_observations_list, [x[l] for x in prediction_means[2]], 'b.', label='prediction center_2')
    #     # axs[i,j].plot(n_observations_list, [x[l] for x in is_means[2]], 'y.', label='is center_2')
    #     # axs[i,j].plot(n_observations_list, [x[l] for x in imis_means[2]], 'r.', label='imis center_2')
    #     axs[i,j].set_xlabel("Observations")
    #     axs[i,j].set_title('X_'+ str(l))
    #     axs[i,j].legend()
    # # TestModel has two solution for the parameter X_2.
    # axs[1,0].plot(n_observations_list, x_obs_2, 'k-', label='observation_2')
    # plt.show()

    # # Plot reconstruction error
    # plt.figure()

    # plt.plot(n_observations_list, prediction_reconstruction_error[0], 'b-', label='prediction mean')
    # # plt.plot(n_observations_list, prediction_reconstruction_error[1], 'b-', label='prediction center_1')
    # # plt.plot(n_observations_list, prediction_reconstruction_error[2], 'b-', label='prediction center_2')
    # plt.plot(n_observations_list, is_reconstruction_error[0], 'y.', label='is mean')
    # # plt.plot(n_observations_list, is_reconstruction_error[1], 'y.', label='is center_1')
    # # plt.plot(n_observations_list, is_reconstruction_error[2], 'y.', label='is center_2')
    # plt.plot(n_observations_list, imis_reconstruction_error[0], 'r.', label='imis mean')
    # # plt.plot(n_observations_list, imis_reconstruction_error[1], 'r.', label='imis center_1')
    # # plt.plot(n_observations_list, imis_reconstruction_error[2], 'r.', label='imis center_2')

    # plt.legend()
    # plt.xlabel('Observations')
    # plt.ylabel(r"$\frac{||y_{pred} - y_{obs}||_2}{||y_{obs}||_2}$")
    # plt.yscale('log')
    # plt.title('Relative reconstruction error')
    # plt.show()

    # for loop
    for i in range(nb_centers+1):
        prediction_reconstruction_error_avg[i].append(np.mean(prediction_reconstruction_error[i]))
        is_reconstruction_error_avg[i].append(np.mean(is_reconstruction_error[i]))
        imis_reconstruction_error_avg[i].append(np.mean(imis_reconstruction_error[i]))
        prediction_error_on_x_avg[i].append(np.mean(prediction_error_on_x[i]))
        is_error_on_x_avg[i].append(np.mean(is_error_on_x[i]))
        imis_error_on_x_avg[i].append(np.mean(imis_error_on_x[i]))

        prediction_reconstruction_error_std[i].append(np.std(prediction_reconstruction_error[i]))
        is_reconstruction_error_std[i].append(np.std(is_reconstruction_error[i]))
        imis_reconstruction_error_std[i].append(np.std(imis_reconstruction_error[i]))
        prediction_error_on_x_std[i].append(np.std(prediction_error_on_x[i]))
        is_error_on_x_std[i].append(np.std(is_error_on_x[i]))
        imis_error_on_x_std[i].append(np.std(imis_error_on_x[i]))

    print(is_error_on_x_avg)
    print(is_error_on_x_std)


# Print average prediction error and reconstruction error
print("Prediction schemes |     K={}".format(K))
print("--------------------------------------------------------------")

# reconstruction errors
print("R(pred-mean)       |     {} ({})".format(np.mean(prediction_reconstruction_error_avg[0]), np.mean(prediction_reconstruction_error_std[0])))
print("R(pred-center_1)   |     {} ({})".format(np.mean(prediction_reconstruction_error_avg[1]), np.mean(prediction_reconstruction_error_std[1])))
print("R(pred-center_2)   |     {} ({})".format(np.mean(prediction_reconstruction_error_avg[2]), np.mean(prediction_reconstruction_error_std[2])))
print("R(IS-mean)         |     {} ({})".format(np.mean(is_reconstruction_error_avg[0]), np.mean(is_reconstruction_error_std[0])))
print("R(IS-center_1)     |     {} ({})".format(np.mean(is_reconstruction_error_avg[1]), np.mean(is_reconstruction_error_std[1])))
print("R(IS-center_2)     |     {} ({})".format(np.mean(is_reconstruction_error_avg[2]), np.mean(is_reconstruction_error_std[2])))
print("R(IMIS-mean)       |     {} ({})".format(np.mean(imis_reconstruction_error_avg[0]), np.mean(imis_reconstruction_error_std[0])))
print("R(IMIS-center_1)   |     {} ({})".format(np.mean(imis_reconstruction_error_avg[1]), np.mean(imis_reconstruction_error_std[1])))
print("R(IMIS-center_2)   |     {} ({})".format(np.mean(imis_reconstruction_error_avg[2]), np.mean(imis_reconstruction_error_std[2])))
print("--------------------------------------------------------------")

# prediction errors (error on X)
print("E(pred-mean)       |     {} ({})".format(np.mean(prediction_error_on_x_avg[0]), np.mean(prediction_error_on_x_std[0])))
print("E(pred-center_1)   |     {} ({})".format(np.mean(prediction_error_on_x_avg[1]), np.mean(prediction_error_on_x_std[1])))
print("E(pred-center_2)   |     {} ({})".format(np.mean(prediction_error_on_x_avg[2]), np.mean(prediction_error_on_x_std[2])))
print("E(IS-mean)         |     {} ({})".format(np.mean(is_error_on_x_avg[0]), np.mean(is_error_on_x_std[0])))
print("E(IS-center_1)     |     {} ({})".format(np.mean(is_error_on_x_avg[1]), np.mean(is_error_on_x_std[1])))
print("E(IS-center_2)     |     {} ({})".format(np.mean(is_error_on_x_avg[2]), np.mean(is_error_on_x_std[2])))
print("E(IMIS-mean)       |     {} ({})".format(np.mean(imis_error_on_x_avg[0]), np.mean(imis_error_on_x_std[0])))
print("E(IMIS-center_1)   |     {} ({})".format(np.mean(imis_error_on_x_avg[1]), np.mean(imis_error_on_x_std[1])))
print("E(IMIS-center_2)   |     {} ({})".format(np.mean(imis_error_on_x_avg[2]), np.mean(imis_error_on_x_std[2])))