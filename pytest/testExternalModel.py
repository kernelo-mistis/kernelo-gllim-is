import os.path
import numpy as np
import math
import kernelo as ker
import matplotlib.pyplot as plt
import json

n_samples = 500

physicalModel = ker.ExternalModelConfig("MyExternalModel", "MyExternalModel", ".").create()

x = np.zeros((n_samples, physicalModel.get_L_dimension()))
n = np.zeros((n_samples, physicalModel.get_D_dimension()))
y = np.zeros((n_samples, physicalModel.get_D_dimension()))

for i in range(n_samples):
    for j in range(physicalModel.get_L_dimension()):
        x[i, j] = 0.4 * math.sin(2.*math.pi*i/n_samples + (j * math.pi/4.)) + 0.5

for i in range(n_samples):
    y[i] = physicalModel.F(x[i])
    n[i] = y[i]/1000.


datas = {'Y_test': {
    'wavelengths': [[i] for i in range(n_samples)],
    'reflectance_test': [[y[i].tolist(), n[i].tolist()] for i in range(n_samples)]
    }
}

with open('result.json', 'w') as fp:
    json.dump(datas, fp)

fig1, axs1 = plt.subplots(1, 2, constrained_layout=True)
fig1.suptitle('predictions by centers')
axs1[0].plot(x, 'c,', label='x')
axs1[1].plot(y, 'b', label='y')
axs1[0].set_title('x')
axs1[1].set_title('y')
axs1[0].set_xlabel('n')
axs1[0].set_ylabel('x')
axs1[0].legend()
axs1[1].set_xlabel('n')
axs1[1].set_ylabel('y')
axs1[1].legend()
plt.show()
