import os.path
import numpy as np
import math
import kernelo as ker
import matplotlib.pyplot as plt
import pickle
import time
import json


# Parameters of the test
dir_path = os.path.dirname(os.path.realpath(__file__))
physical_model_name = "Shkuratov"
n_observations = 10000
n_test = 100

# Creation of CPP model
geometries = []
mukundpura_geometries = {
    "sza": [0.,0,0,0,0,0,0,0,0,0,0,0,0,0,20,20,20,20,20,20,20,20,20,20,20,20,20,20,40,40,40,40,40,40,40,40,40,40,40,40,40,40,60,60,60,60,60,60,60,60,60,60,60,60,60,60,20,20,20,20,20,20,20,20,20,20,20,20,20,20],
    "vza": [70.,60,50,40,30,20,10,10,20,30,40,50,60,70,70,60,50,40,30,20,10,0,10,30,40,50,60,70,70,60,50,30,20,10,0,10,20,30,40,50,60,70,70,60,50,40,30,20,10,0,10,20,30,40,50,70,70,60,50,40,30,10,0,10,20,30,40,50,60,70],
    "phi": [0.,0,0,0,0,0,0,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,0,0,0,0,0,0,0,0,0,0,0,0,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,0,0,0,0,0,0,30,30,30,30,30,30,150,150,150,150,150,150,150,150]
}
geometries.append(mukundpura_geometries["sza"])
geometries.append(mukundpura_geometries["vza"])
geometries.append(mukundpura_geometries["phi"])
geometries = np.array(geometries).T # transpose geometries for Shkuratov.cpp model
scalingCoeffs = [1.0,1.5,1.5,1.5,1.5]
offset = [0,0,0.2,0,0]
variant = '5p'
cppModel = ker.ShkuratovModelConfig(geometries, variant, scalingCoeffs, offset).create()

# Creation of Python model
pythonModel = ker.ExternalModelConfig(physical_model_name, physical_model_name.lower(), dir_path + "/models/").create()

x_obs = np.zeros((n_observations, pythonModel.get_L_dimension()))

for i in range(n_observations):
    for j in range(pythonModel.get_L_dimension()):
        x_obs[i, j] = 0.4 * math.sin(2.*math.pi*i/n_observations + (j * math.pi/4.)) + 0.5

obs_list = [10, 100, 500, 1000, 5000, 10000]
cpp_time_list = []
python_time_list = []

for nb_obs in obs_list:
    ts = time.time()
    for i in range(nb_obs):
        y_pred = cppModel.F(x_obs[i])
    cpp_time_list.append(time.time() - ts)

    ts = time.time()
    for i in range(nb_obs):
        y_pred = pythonModel.F(x_obs[i])
    python_time_list.append(time.time() - ts)

plt.figure()
plt.plot(obs_list, cpp_time_list, 'r', label='CPP')
plt.plot(obs_list, python_time_list, 'b', label='Python')
plt.legend()
plt.xlabel('Number of observation calculatd')
plt.ylabel('Computation time (sec)')
plt.title('Comparison CPP/Python implementation of F(x) computation time')
plt.show()
