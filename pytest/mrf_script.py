import numpy as np
import matplotlib.pyplot as plt
import os.path
import kernelo as ker
import time
import pickle

# GLLiM params
D = 780
L = 7
# seed = int.from_bytes(os.urandom(4), 'little')
seed = 12345
gllim = {
    "K": 50,
    "gamma_type": "Full",
    "sigma_type": "Iso",
    "init_config": {
        "multi": {
            "type": "multi",
            "config": {
                "seed": seed,
                "nb_iter_em": 5,
                "nb_exp": 10,
                "gmm_learning_config": {
                    "kmeans_iteration": 5,
                    "em_iteration": 10,
                    "floor": 1e-8
                }
            }
        },
        "fixed": {
            "type": "fixed",
            "config": {
                "seed": seed,
                "gmm_learning_config": {
                    "kmeans_iteration": 5,
                    "em_iteration": 10,
                    "floor": 1e-8
                }
            }
        },
    },
    "train_config": {
        "em": {
            "type": "em",
            "config": {
                "max_iteration": 20,
                "ratio_ll": 0.00001,
                "floor": 1e-8
            }
        },
        "gmm": {
            "type": "gmm",
            "config": {
                "kmeans_iteration": 10,
                "em_iteration": 10,
                "floor": 1e-8
            }
        }
    }
}

# EM training method
train_context = gllim["train_config"]["em"]["config"]
learningConfig = ker.EMLearningConfig(train_context["max_iteration"], train_context["ratio_ll"], train_context["floor"])

# # GMM training method
# train_context = gllim["train_config"]["em"]["config"]
# learningConfig = ker.GMMLearningConfig(train_context["kmeans_iteration"], train_context["em_iteration"], train_context["floor"])

# MULTI initialisation method
init_context = gllim["init_config"]["multi"]["config"]
gmmInitConfig = ker.GMMLearningConfig(init_context["gmm_learning_config"]["kmeans_iteration"], init_context["gmm_learning_config"]["em_iteration"], init_context["gmm_learning_config"]["floor"])
initConfig = ker.MultInitConfig(init_context["seed"], init_context["nb_iter_em"], init_context["nb_exp"], gmmInitConfig)

# # FIXED initialisation method
# init_context = gllim["init_config"]["fixed"]["config"]
# gmmInitConfig = ker.GMMLearningConfig(init_context["gmm_learning_config"]["kmeans_iteration"], init_context["gmm_learning_config"]["em_iteration"], init_context["gmm_learning_config"]["floor"])
# initConfig = ker.MultInitConfig(init_context["seed"], gmmInitConfig)

gllim = ker.GLLiM(D, L, gllim["K"], gllim["gamma_type"], gllim["sigma_type"], initConfig, learningConfig)

cwd = os.getcwd()
gllim_model_file_name = cwd + "/pytest/gllim_K10_diag_90_v2.file"
with open(gllim_model_file_name, "rb") as f:
    gllim_parameters = pickle.load(f)
    gllim.importModel(gllim_parameters)
print("GLLIM model loaded")


pred_config = {
    "k_merged": 2,
    "k_pred_means": 2,
    "threshold": 1e-10
}
nb_centers = pred_config["k_merged"]

# load signal
cwd = os.getcwd()
signals_file_name = cwd + "/pytest/MRSignals.file"
with open(signals_file_name, "rb") as f:
    signals = pickle.load(f)

print(signals.shape)

### CREATE PREDICTOR
predicator = ker.PredictionConfig(pred_config["k_merged"], pred_config["k_pred_means"], pred_config["threshold"], gllim).create()


### Prediction Naive
predictions = []
t0 = time.time()
for i in range(signals.shape[0]):
    signal = signals[i]
    incertitude = np.ones(signals.shape[1])*1e-5
    predictions.append(predicator.predict(signal, incertitude))
t = time.time() - t0
print("prediction time : {}".format(t))

print("FINI")
