import numpy as np
import mat73 # loading matlab 7.3 files
import os.path
import pickle
import time
import logging
import matplotlib.pyplot as plt
import random
import json
logging.getLogger().setLevel(logging.INFO)
import kernelo as ker

def compute_reconstruction_error(reconstruction, observation):
    return np.linalg.norm(observation - reconstruction) / np.linalg.norm(observation)

def compute_error_on_x(pred, obs_1):
    return np.linalg.norm(pred - obs_1, ord=np.inf)

def logit(u):
    return np.log(u/(1-u))

def logit_inv(v):
    return 1/(1+np.exp(-v))

gllim_name = "basalt_Hapke4p_K10_logit"

dico_name = "BRDF_reflectance_20110414_basalt_hawaii.json"

# pca_components_name = dico_name + '_components'

#Read geometries and photometries from file 'test_hapke.json'
path = "pytest/" + dico_name
with open(path) as json_file:
    data = json.load(json_file)
data = data['reflectance_20110414_basalt_hawaii']
y = np.array(data['reflectance_20110413_basalt_hawaii_coarse'])
geom = np.array(data['geometries'], dtype=float)
# photom = np.array([data['omega'], [x / 30 for x in data['theta0']], data['b'], data['c'], data['b0'], data['hh']]).transpose()

n_observations = y.shape[0]

myFourParamsAdapter = ker.FourParamsHapkeAdapterConfig(0., 0.1)
my02Model = ker.HapkeModelConfig("2002", myFourParamsAdapter, geom, 30.).create()


sigma = 0.01
variances = np.ones(my02Model.get_D_dimension()) * sigma
myStatModel_2 = ker.GaussianStatModelConfig("sobol", my02Model, variances, 12345).create()
x_gen, y_gen = myStatModel_2.gen_data(20000)
y_test = my02Model.F(x_gen[0,:])
# print(y_test)
# print(y_gen[0,:])

MRSignals = y_gen
Parameters = x_gen

# [TEST] LOGIT on Parameters
Parameters = logit(Parameters)


n_signals = MRSignals.shape[0]
n_pulses = MRSignals.shape[1]
n_parameters = Parameters.shape[1]
parameters_label = ['omega', 'theta0', 'b', 'c', 'b0', 'hh']

print(('Loading of a dictionary of {} signals of length {} pulses. \nThere are {} parameters : {}.').format(n_signals, n_pulses, n_parameters, parameters_label))

seed = 12345

gllim_conf = {
    "K": 10,
    "gamma_type": "Full",
    "sigma_type": "Diag",
    "init_config": {
        "multi": {
            "type": "multi",
            "config": {
                "seed": seed,
                "nb_iter_em": 10,
                "nb_exp": 10,
                "gmm_learning_config": {
                    "kmeans_iteration": 5,
                    "em_iteration": 10,
                    "floor": 1e-12
                }
            }
        },
        "fixed": {
            "type": "fixed",
            "config": {
                "seed": seed,
                "gmm_learning_config": {
                    "kmeans_iteration": 5,
                    "em_iteration": 10,
                    "floor": 1e-8
                }
            }
        },
    },
    "train_config": {
        "em": {
            "type": "em",
            "config": {
                "max_iteration": 200,
                "ratio_ll": 0.00001,
                "floor": 1e-12
            }
        },
        "gmm": {
            "type": "gmm",
            "config": {
                "kmeans_iteration": 10,
                "em_iteration": 100,
                "floor": 1e-8
            }
        }
    }
}

# GLLiM params
D = n_pulses
L = n_parameters

# EM training method
train_context = gllim_conf["train_config"]["em"]["config"]
learningConfig = ker.EMLearningConfig(train_context["max_iteration"], train_context["ratio_ll"], train_context["floor"])

# # GMM training method
# train_context = gllim_conf["train_config"]["gmm"]["config"]
# learningConfig = ker.GMMLearningConfig(train_context["kmeans_iteration"], train_context["em_iteration"], train_context["floor"])

# MULTI initialisation method
init_context = gllim_conf["init_config"]["multi"]["config"]
gmmInitConfig = ker.GMMLearningConfig(init_context["gmm_learning_config"]["kmeans_iteration"], init_context["gmm_learning_config"]["em_iteration"], init_context["gmm_learning_config"]["floor"])
initConfig = ker.MultInitConfig(init_context["seed"], init_context["nb_iter_em"], init_context["nb_exp"], gmmInitConfig)

# # FIXED initialisation method
# init_context = gllim["init_config"]["fixed"]["config"]
# gmmInitConfig = ker.GMMLearningConfig(init_context["gmm_learning_config"]["kmeans_iteration"], init_context["gmm_learning_config"]["em_iteration"], init_context["gmm_learning_config"]["floor"])
# initConfig = ker.MultInitConfig(init_context["seed"], gmmInitConfig)

gllim = ker.GLLiM(D, L, gllim_conf["K"], gllim_conf["gamma_type"], gllim_conf["sigma_type"], initConfig, learningConfig)

gllim_model_file_name = "pytest/" + gllim_name + ".file"
if not os.path.isfile(gllim_model_file_name):  # This is very useful to not train Gllim model at every simulation

    print("Initializing GLLIM model")
    t0 = time.time()
    gllim.initialize(Parameters, MRSignals)
    tf = time.time() - t0
    print(("Initialisation step took {} min {} sec").format(tf//60, tf%60))

    print("Training model")
    t0 = time.time()
    gllim.train(Parameters, MRSignals)
    tf = time.time() - t0
    print(("Training step took {} min {} sec").format(tf//60, tf%60))

    gllim_parameters = gllim.exportModel() # you can export your gllim model parameters
    with open(gllim_model_file_name, "wb") as f:
        pickle.dump(gllim_parameters, f, pickle.HIGHEST_PROTOCOL)
    f.close()
    print("GLLIM model saved")
else:
    with open(gllim_model_file_name, "rb") as f:
        gllim_parameters = pickle.load(f)
        gllim.importModel(gllim_parameters)
    print("GLLIM model loaded")


n_observations = 100
alpha_obs = 1000.

x_obs = np.zeros((n_observations, L))
y_obs = np.zeros((n_observations, D))
y_obs_noised = np.zeros((n_observations, 2, D))
y_obs_noise = np.zeros((n_observations, D))

for i in range(n_observations):
    for j in range(L):
        x_obs[i, j] = 0.1 * np.sin(2.*np.pi*i/n_observations + (j * np.pi/4.)) + 0.2
for i in range(n_observations):
    y_obs[i] = np.nan_to_num(my02Model.F(x_obs[i]))
    # Add noise for each Y component
    for j in range(D):
        y_obs_noise[i][j] = np.random.normal(0, pow(y_obs[i][j]/alpha_obs, 2))
y_obs_noised[:,0,:] = y_obs
y_obs_noised[:,1,:] = y_obs_noise

pred_config = {
    "k_merged": 2,
    "k_pred_means": 2,
    "threshold": 1e-10
}

nb_centers = pred_config["k_merged"]
null_pred_dim = (
    L,
    pred_config["k_pred_means"],
    pred_config["k_merged"]
)


# Gllim
predicator = ker.PredictionConfig(nb_centers, nb_centers, 1e-10, gllim).create()
prediction_means = [[] for i in range(nb_centers+1)] # list[0] is the list for mean ; list[1] is the list for center1 ; list[2] is the list for center2
prediction_reconstruction_error = [[] for i in range(nb_centers+1)]
prediction_error_on_x = [[] for i in range(nb_centers+1)]
prop_laws = [[] for i in range(nb_centers+1)] # Proposition laws for IS and IMIS
print("Computing predictions")
for i in range(n_observations):
    prediction = predicator.predict(y_obs_noised[i,0,:], y_obs_noised[i,1,:])
    x_pred = prediction.meansPred.mean    
    # [TEST] LOGIT
    x_pred = logit_inv(x_pred)
    y_pred = my02Model.F(x_pred)
    prediction_means[0].append(x_pred)
    prediction_reconstruction_error[0].append(compute_reconstruction_error(y_pred, y_obs_noised[i,0,:]))
    prediction_error_on_x[0].append(compute_error_on_x(x_pred, x_obs[i]))
    ### Prop law forced between [0,1] ###
    # prediction.meansPred.gmm_weights[prediction.meansPred.gmm_weights < 0] = 1e-5
    # prediction.meansPred.gmm_weights[prediction.meansPred.gmm_weights > 1] = 1 - 1e-5
    # prediction.meansPred.gmm_means[prediction.meansPred.gmm_means < 0] = 1e-5
    # prediction.meansPred.gmm_means[prediction.meansPred.gmm_means > 1] = 1 - 1e-5
    # prediction.meansPred.gmm_covs[prediction.meansPred.gmm_covs < 0] = 1e-5
    # prediction.meansPred.gmm_covs[prediction.meansPred.gmm_covs > 1] = 1 - 1e-5
    ###
    mean_prop_law = ker.GaussianMixturePropositionConfig( # Proposition law for IS and IMIS
        prediction.meansPred.gmm_weights, 
        logit_inv(prediction.meansPred.gmm_means), 
        # prediction.centersPred.means,
        prediction.meansPred.gmm_covs).create()
    prop_laws[0].append(mean_prop_law)

    for center in range(1, nb_centers+1):
        x_pred = logit_inv(prediction.centersPred.means[:, center-1])
        y_pred = my02Model.F(x_pred)
        prediction_means[center].append(x_pred)
        prediction_reconstruction_error[center].append(compute_reconstruction_error(y_pred, y_obs_noised[i,0,:]))
        prediction_error_on_x[center].append(compute_error_on_x(x_pred, x_obs[i]))
        ### Prop law forced between [0,1] ###
        # prediction.centersPred.means[:, center-1][prediction.centersPred.means[:, center-1] < 0] = 1e-5
        # prediction.centersPred.means[:, center-1][prediction.centersPred.means[:, center-1] > 1] = 1 - 1e-5
        ###
        center_prop_law = ker.GaussianRegularizedPropositionConfig(
            logit_inv(prediction.centersPred.means[:, center-1]),
            # prediction.meansPred.gmm_means[:, center-1],
            prediction.centersPred.covs[center-1, :, :]).create()
        prop_laws[center].append(center_prop_law)



sampler_is = ker.ImportanceSamplingConfig(1000, myStatModel_2).create()
is_means = [[] for i in range(nb_centers+1)]
is_reconstruction_error = [[] for i in range(nb_centers+1)]
is_error_on_x = [[] for i in range(nb_centers+1)]
incertitudes = np.ones(n_pulses)*1e-5
print("Computing IS")
for i in range(n_observations-98):
    result = sampler_is.execute(prop_laws[0][i], y_obs_noised[i,0,:], y_obs_noised[i,1,:])
    x_pred = result.mean
    y_pred = my02Model.F(x_pred)
    is_means[0].append(x_pred)
    is_reconstruction_error[0].append(compute_reconstruction_error(y_pred, y_obs_noised[i,0,:]))
    is_error_on_x[0].append(compute_error_on_x(x_pred, x_obs[i]))

    for center in range(1, nb_centers+1):
        result = sampler_is.execute(prop_laws[center][i], y_obs_noised[i,0,:], y_obs_noised[i,1,:])
        x_pred = result.mean
        y_pred = my02Model.F(x_pred)
        is_means[center].append(x_pred)
        is_reconstruction_error[center].append(compute_reconstruction_error(y_pred, y_obs_noised[i,0,:]))
        is_error_on_x[center].append(compute_error_on_x(x_pred, x_obs[i]))
