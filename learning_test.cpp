//
// Created by reverse-proxy on 12‏/2‏/2020.
//


#include <armadillo>
#include <utility>
#include "omp.h"

#include "src/learningModel/configs/InitConfig.h"
#include "src/learningModel/gllim//GLLiMLearning.h"
#include "src/learningModel/initializers/FixedInitializer.h"
#include "src/learningModel/initializers/MultInitializer.h"
#include "src/learningModel/gllim/GLLiMParameters.h"
#include "src/learningModel/configs/LearningConfig.h"
#include "src/learningModel/estimators/GmmEstimator.h"
#include "src/learningModel/estimators/EmEstimator.h"
#include "src/learningModel/covariances/Icovariance.h"
#include "src/dataGeneration/SobolGenerator.h"
#include "src/dataGeneration/RandomGenerator.h"
#include "src/prediction/Predictor.h"
#include "src/functionalModel/FunctionalModel.h"
#include "src/functionalModel/HapkeModel/HapkeVersions/Hapke02Model.h"
#include "src/functionalModel/HapkeModel/HapkeAdapters/FourParamsModel.h"
#include "src/functionalModel/HapkeModel/HapkeAdapters/ThreeParamsModel.h"
#include "src/functionalModel/HapkeModel/HapkeAdapters/SixParamsModel.h"
#include "src/dataGeneration/LatinCubeGenerator.h"
#include "src/dataGeneration/creators.h"


#include <iostream>
#include <cstring>
#include <exception>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <fstream>


using namespace std;
namespace pt = boost::property_tree;

using namespace learningModel;
using namespace arma;

/*int main(){

    auto *geometries = new double[50*3];
    unsigned i = 0;


    pt::ptree root;
    pt::read_json("../test_hapke.json", root);  // Load the json file in this ptree
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("eme"))
    {
        geometries[i*3+0] = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("inc"))
    {
        geometries[i*3+1] = stod(v.second.data());

        i++;
    }
    i = 0;



    for (pt::ptree::value_type& v : root.get_child("phi"))
    {
        geometries[i*3+2] = stod(v.second.data());
        i+=1;
    }



    mat photometries = mat(10000,6);

    i = 0;
    for (pt::ptree::value_type& v : root.get_child("omega"))
    {
        photometries(i,0) = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("b"))
    {
        photometries(i,2) = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("c"))
    {
        photometries(i,3) = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("hh"))
    {
        photometries(i,5) = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("b0"))
    {
        photometries(i,4) = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("theta0"))
    {
        photometries(i,1) = stod(v.second.data()) / 30.0;
        i++;
    }


    mat y = mat(10000,50);
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("y"))
    {
        int j = 0;
        for(pt::ptree::value_type& elem : v.second){
            y(i,j) = elem.second.get_value<double>();
            j++;
        }
        i++;
    }
    //for(unsigned t=0; t<10; t++){


        y += (mat(y.n_rows, y.n_cols, fill::randn) * 1/100);
        //y = y.submat(0,0,y.n_rows-1,4);
        //photometries = mat(100, 2, fill::randn);

        arma_rng::set_seed_random();
        mat T_L = trimatl(mat(photometries.n_cols, photometries.n_cols, fill::randn));
        mat S_L = T_L * T_L.t();
        S_L.diag() += 1;
        arma_rng::set_seed_random();

        mat T_D = trimatl(mat(y.n_cols, y.n_cols, fill::randn));
        mat S_D = T_D * T_D.t();
        S_D.diag() += 1;
        arma_rng::set_seed_random();

        GLLiMParameters<FullCovariance,FullCovariance> myParams = GLLiMParameters<FullCovariance,FullCovariance>();

        int K = 20;

        myParams.A = cube(y.n_cols, photometries.n_cols, K);
        myParams.B = mat(y.n_cols, K);
        myParams.C = mat(photometries.n_cols, K);
        myParams.Pi = normalise(vec(K, fill::randn), 1);
        myParams.Sigma = std::vector<FullCovariance>(K);
        myParams.Gamma = std::vector<FullCovariance>(K);

        //S_D.print("Sigma");
        //S_L.print("Gamma");


        for(unsigned p=0; p < K ; p++ ){
            myParams.Sigma[p] = FullCovariance(S_D);
            myParams.Gamma[p] = FullCovariance(S_L);

            myParams.A.slice(p) = mat(y.n_cols, photometries.n_cols, fill::randn);
            arma_rng::set_seed_random();
            myParams.B.col(p) = vec(y.n_cols, fill::randn);
            arma_rng::set_seed_random();
            myParams.C.col(p) = vec(photometries.n_cols, fill::randn);
            arma_rng::set_seed_random();
        }




        //std::shared_ptr<GMMLearningConfig> myLearningconfig (new GMMLearningConfig(0,10));
        //GmmEstimator estimator (myLearningconfig);

        std::shared_ptr<EMLearningConfig> myLearningconfig (new EMLearningConfig(10,1));
        EmEstimator<FullCovariance, FullCovariance> estimator(myLearningconfig);

        vec l = ones<vec>(5);
        vec m = ones<vec>(5);
        //l.print("l");
        //m.print("m");
        //std::cout << dot(l, m) << std::endl;



        //auto start = chrono::high_resolution_clock::now();
        estimator.estimate(photometries, y, myParams);
        //auto end = chrono::high_resolution_clock::now();
        //auto duration = chrono::duration_cast<chrono::seconds>(end - start);
        //cout << duration.count() << endl;
   // }





}*/

int main(){



    int L = 6;
    int D = 47;
    int N = 10000;
    int K = 50;

    // Fixer A et B
    arma_rng::set_seed(10000);
    mat A(D,L, fill::randu);
    arma_rng::set_seed(11000);
    vec B(D, fill::randu);

    // Fixer sigma
    mat sigma(N,D, fill::randn);
    sigma *= sqrt(0.01);

    // Fixer C, Gamma et Pi
    arma_rng::set_seed(11100);
    vec Pi = normalise(vec(K, fill::randu), 1);

    arma_rng::set_seed(11110);
    mat C(L,K, fill::randu);

    cube Gamma(L,L,K);


    for(unsigned p=0; p < K ; p++ ){
        arma_rng::set_seed_random();
        mat T_L = trimatl(mat(L, L, fill::randu));
        Gamma.slice(p) = T_L * T_L.t();
        Gamma.slice(p).diag() += 1;
    }


    // init gmm
    gmm_full model;
    model.set_params(C, Gamma, Pi.t());

    // sample X
    mat X = model.generate(N);

    // Calculer Y
    mat Y = mat(A * X);
    for(unsigned n=0; n<N; n++){
        for(unsigned d=0; d<D; d++){
            Y(d,n) += B(d) + sigma(n,d);
        }

    }



    /*mat u(D,D,fill::zeros);
    for(unsigned n=0; n<N; n++){
        mat temp = mat(Y.col(n) - A * X.col(n) - B);

        u = u + temp * temp.t();
    }

    u /= N;
    //std::cout << sum(u) << std::endl;
    u.print();*/

    arma_rng::set_seed_random();
    mat T_L = trimatl(mat(L, L, fill::randu));
    mat S_L = T_L * T_L.t();
    S_L.diag() += 1;
    arma_rng::set_seed_random();

    mat T_D = trimatl(mat(D, D, fill::randu));
    mat S_D = T_D * T_D.t();
    S_D.diag() += 1;
    arma_rng::set_seed_random();


    GLLiMParameters<FullCovariance,FullCovariance> myParams = GLLiMParameters<FullCovariance,FullCovariance>(D,L,K);
    myParams.Pi = normalise(vec(K, fill::randu), 1);

    mat sig(D,D, fill::zeros);
    sig.diag() += 0.01;
    //sig.print();

    for(unsigned p=0; p < K ; p++ ){

       /* myParams.Sigma[p] = FullCovariance(sig);
        myParams.Gamma[p] = FullCovariance(Gamma.slice(p));
        myParams.A.slice(p) = A;
        myParams.B.col(p) = B;
        myParams.C.col(p) = C.col(p);*/

        myParams.Sigma[p] = FullCovariance(S_D);
        myParams.Gamma[p] = FullCovariance(S_L);

        myParams.A.slice(p) = mat(D,L, fill::randu);
        arma_rng::set_seed_random();
        myParams.B.col(p) = vec(D, fill::randu);
        arma_rng::set_seed_random();
        myParams.C.col(p) = vec(L, fill::randu);
        arma_rng::set_seed_random();
    }


    myParams.Sigma[0].print();
   /* myParams.A.print("Init A");
    myParams.B.t().print("Init B");
    myParams.C.print("Init C");

    myParams.Pi.print("Init Pi");*/


    auto *geometries = new double[50*3];
    unsigned i = 0;


    pt::ptree root;
    pt::read_json("../cpptest/functionalModel_tests/test_hapke.json", root);  // Load the json file in this ptree
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("eme"))
    {
        geometries[i*3+0] = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("inc"))
    {
        geometries[i*3+1] = stod(v.second.data());

        i++;
    }
    i = 0;



    for (pt::ptree::value_type& v : root.get_child("phi"))
    {
        geometries[i*3+2] = stod(v.second.data());
        i+=1;
    }



    mat photometries = mat(10000,6);

    i = 0;
    for (pt::ptree::value_type& v : root.get_child("omega"))
    {
        photometries(i,0) = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("b"))
    {
        photometries(i,2) = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("c"))
    {
        photometries(i,3) = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("hh"))
    {
        photometries(i,5) = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("b0"))
    {
        photometries(i,4) = stod(v.second.data());
        i++;
    }
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("theta0"))
    {
        photometries(i,1) = stod(v.second.data()) / 30.0;
        i++;
    }


    mat y = mat(10000,50);
    i = 0;
    for (pt::ptree::value_type& v : root.get_child("y"))
    {
        int j = 0;
        for(pt::ptree::value_type& elem : v.second){
            y(i,j) = elem.second.get_value<double>();
            j++;
        }
        i++;
    }



    y += (mat(y.n_rows, y.n_cols, fill::randn) * 1/100);




    // testing initialization
    std::shared_ptr<MultInitConfig> myConfig (
            new MultInitConfig(
                    123456789,
                    3,
                    1,
                    make_shared<GMMLearningConfig>(GMMLearningConfig(0,4,1e-10)),
                    make_shared<EMLearningConfig>(EMLearningConfig(3,0,1e-08))));

    MultInitializer<FullCovariance,FullCovariance> initializer(myConfig);
    //std::shared_ptr<GLLiMParameters <FullCovariance, FullCovariance>> gllim_initialized = initializer.execute(photometries.submat(0,0,N-1,L-1), y.submat(0,3,N-1,49),K);

    /*std::shared_ptr<GMMLearningConfig> myLearningconfig (new GMMLearningConfig(0,10));
    GmmEstimator estimator (myLearningconfig);*/

    std::shared_ptr<GMMLearningConfig> myLearningconfig (new GMMLearningConfig(0,5,0.00000001));
    GmmEstimator estimator (myLearningconfig);
    //EmEstimator<FullCovariance, DiagCovariance> estimator(myLearningconfig);

    /*auto start = chrono::high_resolution_clock::now();

    estimator.execute(photometries.submat(0,0,N-1,L-1), y.submat(0,3,N-1,49), gllim_initialized);

    auto end = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::seconds>(end - start);
    cout << duration.count() << endl;*/

    /*std::shared_ptr<IGLLiMLearning> gllim (
            new GLLiMLearning<FullCovariance,FullCovariance>(
                    make_shared<MultInitializer<FullCovariance,FullCovariance>>(initializer),
                    make_shared<GmmEstimator>(estimator),50));

    std::shared_ptr<Functional::FunctionalModel> myModel (new Hapke02Model(geometries, 50, 3, std::shared_ptr<HapkeAdapter>(new SixParamsModel()), 30.0));
    std::shared_ptr<DataGeneration::StatModel> statModel = DataGeneration::DependentGaussianStatModelConfig("sobol", myModel,20, 123456789).create();

    std::tuple<mat, mat> gen = statModel->gen_data(10000);

    std::cout << std::get<1>(gen).max() << std::endl;

    gllim->initialize(std::get<0>(gen), std::get<1>(gen));
    gllim->train(std::get<0>(gen),std::get<1>(gen));*/
    /*vec cov_obs(47, fill::randu);
    vec y_obs = y.row(5).subvec(3,49).t();

    prediction::Predictor predictor(gllim, 2, 1e-10);

    std::vector<std::pair<vec,vec>> centers;

    auto start = chrono::high_resolution_clock::now();
    for(unsigned n=0; n<1; n++){
        centers = predictor.predict(y_obs, cov_obs);
        for(const auto center : centers){
            photometries.row(5).print();
            center.first.t().print();
            std::cout << max(arma::abs(center.first.t() - photometries.row(5))) << std::endl;
        }
    }
    auto end = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::seconds>(end - start);*/
    //cout << duration.count() << endl;


    auto *test = new double[5*3*2];
    vec a(&test[0], 15, false, true);
    mat b(&test[0], 5, 3, false, true);
    cube c(&test[0], 5,3,2, false, true);

    for(unsigned i=0; i<30; i++){
        test[i] = i*1.0;
    }

    a.print();
    b.print();
    c.print();



}

