//
// Created by sam on 03/05/22.
//

#include "../../src/functionalModel/TestModel/TestModel.h"
#include <gtest/gtest.h>
#include <armadillo>
#include <iostream>


namespace Functional{
    using namespace std;

    class TestModelTest : public ::testing::Test{
    protected:
        void SetUp() override {
            model = unique_ptr<TestModel>((new TestModel()));
        }

        unique_ptr<TestModel> model;
    };

    TEST_F(TestModelTest, test1){
        rowvec x = {0.2, 0.5, 0.9, 0.1};
        x.print();
        rowvec y(9, fill::ones);
        model->F(x, y);
        y.print();
    }
}