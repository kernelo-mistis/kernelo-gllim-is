//
// Created by reverse-proxy on 13‏/3‏/2020.
//

#include <gtest/gtest.h>
#include <armadillo>
#include <iostream>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include "../../src/functionalModel/ShkuratovModel/ShkuratovModel.h"



namespace Functional{
    using namespace ShkuratovEnumeration;
    using namespace std;
    namespace pt = boost::property_tree;

    class ShkuratovModelTest : public ::testing::Test{
    protected:
        void SetUp() override {
            unsigned i = 0;
            pt::ptree root;
            pt::read_json("cpptest/functionalModel_tests/new_test_shkuratov.json", root);  // Load the json file in this ptree

            //Read geometries
            std::string variables[] = {"inc", "eme", "phi"};
            for(unsigned j=0; j<3; j++){
                i=0;
                for (pt::ptree::value_type& v : root.get_child(variables[j]))
                {
                    geometries[i*3+j] = stod(v.second.data());
                    i++;
                }
            }
            model = unique_ptr<ShkuratovModel>((new ShkuratovModel(geometries, 50, 3, variant, scaling, offset)));
        };

        unique_ptr<ShkuratovModel> model;
        unsigned D = 50;
        double geometries[50*3];
        double scaling[5] = {1.0,1.5,1.5,1.5,1.5};
        double offset[5] = {0,0,0.2,0,0};
        std::string variant = "5p";

    };

    TEST_F(ShkuratovModelTest, F){
        unsigned N = 10000;
        unsigned L = 5;
        pt::ptree root;
        pt::read_json("cpptest/functionalModel_tests/new_test_shkuratov.json", root);
        std::string variables[] = {"an", "mu1", "nu", "m", "mu2"};
        mat photometries = mat(N,L);
        int n=0;

        // Read photometries
        for(unsigned l=0; l < L; l++){
            n=0;
            for (pt::ptree::value_type& v : root.get_child(variables[l]))
            {
                photometries(n, l) = (stod(v.second.data()) - offset[l]) / scaling[l];
                n++;
            }
        }

        // Read expected results
        mat expected_results(N,D);
        n=0;
        for (pt::ptree::value_type& v : root.get_child("y"))
        {
            int l=0;
            for (pt::ptree::value_type& w : v.second){
                expected_results(n, l) = stod(w.second.data());
                l++;
            }
            n++;
        }

        // compute results from the model
        rowvec result(D);
        for(unsigned n=0; n<N; n++){
            model->F(photometries.row(n), result);
            ASSERT_TRUE(approx_equal(expected_results.row(n), result, "reldiff", 1e-8));
        }
    }
}

