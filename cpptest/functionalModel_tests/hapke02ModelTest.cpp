//
// Created by reverse-proxy on 27‏/12‏/2019.
//

#include "../../src/functionalModel/HapkeModel/HapkeVersions/Hapke02Model.h"
#include "../../src/functionalModel/HapkeModel/HapkeAdapters/SixParamsModel.h"
#include "../../src/functionalModel/HapkeModel/HapkeAdapters/FourParamsModel.h"
#include <gtest/gtest.h>
#include <armadillo>
#include <iostream>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>





namespace Functional{

    using namespace HapkeEnumeration;
    using namespace std;
    namespace pt = boost::property_tree;

    class Hapke02ModelTest : public ::testing::Test{
    protected:
        void SetUp() override {
            unsigned i = 0;

            //Read geometries for 6 parameters model
            pt::read_json("../../../data_ref_hapke6p_geom70.json", root_6params);  // Load the json file in this ptree
            i=0;
            unsigned int p = 0;
            for (pt::ptree::value_type& v : root_6params.get_child("data_ref").get_child("geometries"))
            {
                p = 0;
                for (auto it: v.second.get_child("")) {
                    geometries_6params[i*3+p] = stod(it.second.data());
                    p++;
                }
                i++;
            }

            //Read geometries for 4 parameters model
            pt::read_json("../../../data_ref_hapke4p_geom70.json", root_4params);  // Load the json file in this ptree

            i=0;
            p = 0;
            for (pt::ptree::value_type& v : root_4params.get_child("data_ref").get_child("geometries"))
            {
                p = 0;
                for (auto it: v.second.get_child("")) {
                    geometries_4params[i*3+p] = stod(it.second.data());
                    p++;
                }
                i++;
            }

            model6params = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries_6params, 70, 3,
                                                                   std::shared_ptr<HapkeAdapter>(new SixParamsModel()),
                                                                   30.));
            model4params = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries_4params, 70, 3,
                                                                          std::shared_ptr<HapkeAdapter>(new FourParamsModel(0, 0.1)),
                                                                          30.));
        }

        pt::ptree root_4params;
        pt::ptree root_6params;
        unique_ptr<Hapke02Model> model6params;
        unique_ptr<Hapke02Model> model4params;
        unsigned D_6params=70;
        unsigned D_4params=70;
        double geometries_6params[70*3];
        double geometries_4params[70*3];
    };

    TEST_F(Hapke02ModelTest, F6params){
        unsigned N = 20000;
        unsigned L = 6;
        mat photometries = mat(N,L);
        int n=0;
        int p=0;

        // Read photometries
        n=0;
        for (pt::ptree::value_type& v : root_6params.get_child("data_ref").get_child("synthetic_dataset").get_child("X"))
        {
            p=0;
            for (auto it: v.second.get_child("")) {
                photometries(n, p) = stod(it.second.data());
                p++;
            }
            n++;
        }

        // Read expected results
        mat expected_results(N,D_6params);
        n=0;
        for (pt::ptree::value_type& v : root_6params.get_child("data_ref").get_child("synthetic_dataset").get_child("Y"))
        {
            p=0;
            for (auto it: v.second.get_child("")) {
                expected_results(n, p) = stod(it.second.data());
                p++;
            }
            n++;
        }

        // compute results from the model
        rowvec result(D_6params);
        for(n=0; n<N; n++){
            model6params->F(photometries.row(n), result);
            ASSERT_TRUE(approx_equal(expected_results.row(n), result, "reldiff", 1e-7));
        }
    }

    TEST_F(Hapke02ModelTest, F4params){
        unsigned N = 20000;
        unsigned L = 4;
        mat photometries = mat(N,L);
        int n=0;
        int p=0;

        // Read photometries
        n=0;
        for (pt::ptree::value_type& v : root_4params.get_child("data_ref").get_child("synthetic_dataset").get_child("X"))
        {
            p=0;
            for (auto it: v.second.get_child("")) {
                photometries(n, p) = stod(it.second.data());
                p++;
            }
            n++;
        }

        // Read expected results
        mat expected_results(N,D_4params);
        n=0;
        for (pt::ptree::value_type& v : root_4params.get_child("data_ref").get_child("synthetic_dataset").get_child("Y"))
        {
            p=0;
            for (auto it: v.second.get_child("")) {
                expected_results(n, p) = stod(it.second.data());
                p++;
            }
            n++;
        }

        // compute results from the model
        rowvec result(D_4params);
        for(n=0; n<N; n++){
            model4params->F(photometries.row(n), result);
            ASSERT_TRUE(approx_equal(expected_results.row(n), result, "reldiff", 1e-7));
        }
    }
}



