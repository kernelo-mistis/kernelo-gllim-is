//
// Created by reverse-proxy on 16‏/3‏/2020.
//

#include <gtest/gtest.h>
#include <memory>
#include "../../src/learningModel/configs/LearningConfig.h"
#include "../../src/learningModel/estimators/EmEstimator.h"

namespace learningModel{

    class EmEstimatorTest : public ::testing::Test{
    protected:

        void SetUp() override {
            unsigned K = 2, N = 10000, L = 2 , D = 5;
            myLearningconfig = std::make_shared<EMLearningConfig>(5,0.0,1e-08);
            estimator = EmEstimator<FullCovariance, FullCovariance>(myLearningconfig);


            // Fixer A et B
            arma_rng::set_seed(10000);
            mat A(D,L, fill::randu);
            arma_rng::set_seed(11000);
            vec B(D, fill::randu);

            // Fixer sigma
            mat Sigma(N, D, fill::randn);
            Sigma *= sqrt(0.01);

            // Fixer C, Gamma et Pi
            arma_rng::set_seed(11100);
            vec Pi = normalise(vec(K, fill::randu), 1);

            arma_rng::set_seed(11110);
            mat C(L,K, fill::randu);

            cube Gamma(L,L,K);

            for(unsigned p=0; p < K ; p++ ){
                arma_rng::set_seed_random();
                mat T_L = trimatl(mat(L, L, fill::randu));
                Gamma.slice(p) = T_L * T_L.t();
                Gamma.slice(p).diag() += 1;
            }

            // init gmm
            gmm_full model;
            model.set_params(C, Gamma, Pi.t());

            // sample X
            X = model.generate(N);

            // Calculer Y
            Y = mat(A * X);
            for(unsigned n=0; n<N; n++){
                for(unsigned d=0; d<D; d++){
                    Y(d,n) += B(d) + Sigma(n, d);
                }
            }

            mat sig(D,D, fill::zeros);
            sig.diag() += 0.01;

            initial_theta = std::make_shared<GLLiMParameters<FullCovariance, FullCovariance>>(D, L, K);
            initial_theta->Pi = normalise(vec(K, fill::randu), 1);
            for(unsigned k=0; k < K ; k++ ){
                initial_theta->Sigma[k] = FullCovariance(sig);
                initial_theta->Gamma[k] = FullCovariance(Gamma.slice(k));
                initial_theta->A.slice(k) = A;
                initial_theta->B.col(k) = B;
                initial_theta->C.col(k) = C.col(k);
            }
        };

        std::shared_ptr<EMLearningConfig> myLearningconfig;
        EmEstimator<FullCovariance, FullCovariance> estimator;
        std::shared_ptr<GLLiMParameters<FullCovariance, FullCovariance>> initial_theta;
        mat X, Y;


    };

    TEST_F(EmEstimatorTest, estimateZeroDetMeans){
        for(unsigned k=0; k < initial_theta->K ; k++ ){
            initial_theta->Sigma[k] = FullCovariance(initial_theta->D);
        }
        estimator.execute(X.t(),Y.t(),initial_theta);

        ASSERT_EQ(accu(initial_theta->Pi) , 0);
    }

    TEST_F(EmEstimatorTest, estimateZeroDetCovariances){
        for(unsigned k=0; k < initial_theta->K ; k++ ){
            initial_theta->Gamma[k] = FullCovariance(initial_theta->L);
        }
        estimator.execute(X.t(),Y.t(),initial_theta);

        ASSERT_EQ(accu(initial_theta->Pi) , 0);
    }

    TEST_F(EmEstimatorTest, estimateZeroDetMeansZeroDetCovariances){
        for(unsigned k=0; k < initial_theta->K ; k++ ){
            initial_theta->Sigma[k] = FullCovariance(initial_theta->D);
            initial_theta->Gamma[k] = FullCovariance(initial_theta->L);
        }
        estimator.execute(X.t(),Y.t(),initial_theta);

        ASSERT_EQ(accu(initial_theta->Pi), 0);
    }

    TEST_F(EmEstimatorTest, estimateZeroWeight){
        initial_theta->Pi(1) = 0;
        estimator.execute(X.t(),Y.t(),initial_theta);
        ASSERT_EQ(initial_theta->Pi(1) , 0);
    }

    TEST_F(EmEstimatorTest, normLogRnk){
        mat log_rnk(20,10, fill::zeros);
        mat norm_log_rnk = estimator.norm_log_rnk(log_rnk);
        mat expected(20,10, fill::ones);
        expected *= -log(10);
        ASSERT_EQ(accu(norm_log_rnk != expected), 0);

        log_rnk = mat(20,10, fill::ones);
        log_rnk *= -datum::inf;
        norm_log_rnk = estimator.norm_log_rnk(log_rnk);
        ASSERT_EQ(accu(norm_log_rnk != log_rnk), 0);
    }



}
