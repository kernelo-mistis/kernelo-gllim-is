//
// Created by reverse-proxy on 18‏/3‏/2020.
//

#include <gtest/gtest.h>
#include <memory>
#include "../../src/learningModel/configs/LearningConfig.h"
#include "../../src/learningModel/estimators/GmmEstimator.h"

namespace learningModel{
    class GmmEstimatorTest : public ::testing::Test{
    protected:
        void SetUp() override  {
            unsigned K = 2, N = 10000, L = 2 , D = 5;
            myLEarningConfig = std::make_shared<GMMLearningConfig>(0,3,1e-08);
            estimator = GmmEstimator(myLEarningConfig);

            mat A_k = mat({
                                {2,0},
                                {0,1},
                                {6,0},
                                {3,2},
                                {5,-1}
                        });

            vec B_k = vec({3,2,6,1,3});
            vec C_k = vec({0,3});
            vec Pi = vec({0.8, 0.2});
            mat sigma_k = mat(D,D, fill::ones);
            sigma_k *= 0.1;
            mat gamma_k = mat(L,L, fill::ones);
            gamma_k *= 0.1;

            theta = std::make_shared<GLLiMParameters<FullCovariance, FullCovariance>>(D, L, K);
            theta->Pi = Pi;
            for(unsigned k=0; k < K ; k++ ){
                theta->Sigma[k] = FullCovariance(sigma_k);
                theta->Gamma[k] = FullCovariance(gamma_k);
                theta->A.slice(k) = A_k;
                theta->B.col(k) = B_k;
                theta->C.col(k) = C_k;
            }
        };

        std::shared_ptr<GMMLearningConfig> myLEarningConfig;
        GmmEstimator estimator;
        std::shared_ptr<GLLiMParameters<FullCovariance, FullCovariance>> theta;
        mat X, Y;
    };

    TEST_F(GmmEstimatorTest , toGMM){
        estimator.toGMM(theta);
        vec Expected_Rou = vec({0.8, 0.2});
        mat Expected_M = mat({
                                     {0,0},
                                     {3,3},
                                     {3,3},
                                     {5,5},
                                     {6,6},
                                     {7,7},
                                     {0,0}
        });
        cube Expected_V = cube(7, 7 , 2);
        for(unsigned k=0; k<2; k++){
            Expected_V.slice(k) = mat({
                                              {0.1, 0.1, 0.2, 0.1, 0.6, 0.5, 0.4},
                                              {0.1, 0.1, 0.2, 0.1, 0.6, 0.5, 0.4},
                                              {0.2, 0.2, 0.5, 0.3, 1.3, 1.1, 0.9},
                                              {0.1, 0.1, 0.3, 0.2, 0.7, 0.6, 0.5},
                                              {0.6, 0.6, 1.3, 0.7, 3.7, 3.1, 2.5},
                                              {0.5, 0.5, 1.1, 0.6, 3.1, 2.6, 2.1},
                                              {0.4, 0.4, 0.9, 0.5, 2.5, 2.1, 1.7},

            });
        }




        ASSERT_EQ(accu( Expected_Rou != estimator.Rou), 0);
        ASSERT_EQ( accu(Expected_M != estimator.M), 0);
        ASSERT_EQ(accu(Expected_V - Expected_V), 0);

    }

    TEST_F(GmmEstimatorTest, fromGMM){
        estimator.Rou = vec({0.8, 0.2});
        estimator.M = mat({
                                     {0,0},
                                     {3,3},
                                     {3,3},
                                     {5,5},
                                     {6,6},
                                     {7,7},
                                     {0,0}
                             });
        estimator.V = cube(7, 7 , 2);
        for(unsigned k=0; k<2; k++){
            estimator.V.slice(k) = mat({
                                              {0.1, 0.0, 0.2, 0.0, 0.6, 0.3, 0.5},
                                              {0.1, 0.1, 0.2, 0.1, 0.6, 0.5, 0.4},
                                              {0.2, 0.0, 0.5, 0.1, 1.3, 0.7, 1.1},
                                              {0.1, 0.1, 0.3, 0.2, 0.7, 0.6, 0.5},
                                              {0.6, 0.0, 1.3, 0.1, 3.7, 1.9, 3.1},
                                              {0.5, 0.2, 1.1, 0.3, 3.1, 2.0, 2.4},
                                              {0.4,-0.1, 0.9, 0.0, 2.5, 1.1, 2.2},
                                      });
        }
        for(unsigned k=0; k < 2 ; k++ ){
            theta->Gamma[k] = FullCovariance(mat({
                                                         {0.1, 0.0},
                                                         {0.1, 0.1}
            }));
        }
        GLLiMParameters<FullCovariance, FullCovariance> calulated_theta = estimator.fromGMM(2,5,2);
        ASSERT_EQ(accu(calulated_theta.Pi - theta->Pi), 0);
        ASSERT_TRUE(approx_equal(calulated_theta.B , theta->B, "absdiff", 1e-15));
        ASSERT_TRUE(approx_equal(calulated_theta.C , theta->C, "absdiff", 1e-15));
        ASSERT_TRUE(approx_equal(calulated_theta.A , theta->A, "absdiff", 1e-15));

        for(unsigned k=0; k < 2 ; k++ ){
            ASSERT_TRUE(approx_equal(calulated_theta.Gamma[k].getFull() , theta->Gamma[k].getFull(), "absdiff", 1e-15));
            ASSERT_TRUE(approx_equal(calulated_theta.Sigma[k].getFull() , theta->Sigma[k].getFull(), "absdiff", 1e-15));
        }

    }

}