//
// Created by reverse-proxy on 9‏/3‏/2020.
//

#include "../../src/learningModel/covariances/Icovariance.h"
#include <gtest/gtest.h>

namespace learningModel{

    class FullCovarianceTest : public ::testing::Test{
    protected:
        void SetUp()override {
            A_arma = mat({
                {1,2,3,4},
                {5,6,7,8},
                {9,10,11,12},
                {13,14,15,16}});

            B_arma = mat({
                    {1,0,0,4},
                    {0,6,0,8},
                    {9,0,11,0},
                    {0,14,0,16}}
            );

        }
        mat A_arma;
        mat B_arma;
        vec V_arma;
        rowvec W_arma;
    };

    TEST_F(FullCovarianceTest, Constructor){
        FullCovariance A_full = FullCovariance(A_arma);
        ASSERT_EQ(accu(A_arma != A_full.getFull()), 0);

        FullCovariance B_full = FullCovariance(5);
        ASSERT_EQ(accu(B_full.getFull() != mat(5,5,fill::zeros)), 0);
    }

    TEST_F(FullCovarianceTest, EqualOperator){
        FullCovariance A_full;
        FullCovariance B_full = FullCovariance(B_arma);

        A_full = A_arma;
        ASSERT_EQ(accu(A_arma != A_full.getFull()), 0);

        A_full = B_full;
        ASSERT_EQ(accu(B_arma != A_full.getFull()), 0);

        A_full = 5;
        mat a_arma(4,4,fill::ones);
        a_arma *= 5;
        ASSERT_EQ(accu(a_arma != A_full.getFull()), 0);
    }

    TEST_F(FullCovarianceTest, IncrementOperator){
        FullCovariance A_full(A_arma);
        A_full += mat(4,4,fill::ones);
        mat result = A_arma + mat(4,4,fill::ones);
        ASSERT_EQ(accu(result != A_full.getFull()), 0);

        A_full += 1;
        result += 1;
        ASSERT_EQ(accu(A_full.getFull() != result), 0);
    }

    TEST_F(FullCovarianceTest, SumOperator){
        mat a_arma(4,4,fill::ones);
        FullCovariance A_full(A_arma);
        mat result_l = A_full + a_arma;
        mat result_r = a_arma + A_full;
        mat expected_result = a_arma + A_arma;

        ASSERT_EQ(accu(result_l != result_r), 0);
        ASSERT_EQ(accu(result_l != expected_result), 0);
    }

    TEST_F(FullCovarianceTest, SubtractionOperator){
        mat a_arma(4,4,fill::ones);
        FullCovariance A_full(A_arma);
        mat result_l = A_full - a_arma;
        mat result_r = a_arma - A_full;
        mat expected_result_l = mat({
            {0,1,2,3},
            {4,5,6,7},
            {8,9,10,11},
            {12,13,14,15}
        });
        mat expected_result_r = mat({
            {0,-1,-2,-3},
            {-4,-5,-6,-7},
            {-8,-9,-10,-11},
            {-12,-13,-14,-15}
        });

        ASSERT_EQ(accu(result_l != expected_result_l), 0);
        ASSERT_EQ(accu(result_r != expected_result_r), 0);
    }

    TEST_F(FullCovarianceTest, ProductOperator){
        FullCovariance A_full(B_arma);
        mat a_arma(4,6,fill::ones);
        mat b_arma(6,4,fill::ones);

        mat result_l = A_full * a_arma;
        mat result_r = b_arma * A_full;
        vec v_result = A_full * vec(4, fill::ones);
        vec v_expected({5, 14, 20, 30});
        rowvec w_result = rowvec(4, fill::ones) * A_full;
        rowvec w_expected({10,20,11,28});


        ASSERT_EQ(accu(result_l != B_arma*a_arma), 0);
        ASSERT_EQ(accu(result_r != b_arma * B_arma), 0);
        ASSERT_EQ( accu(v_result != v_expected), 0);
        ASSERT_EQ(accu(w_result != w_expected), 0);

    }

    TEST_F(FullCovarianceTest, rankOneUpdate){
        FullCovariance A_full(B_arma);
        A_full.rankOneUpdate(vec(4, fill::ones), 0.1);
        mat result = A_full.getFull();
        mat expected_result = B_arma + mat(4,4,fill::ones) * 0.1;

        ASSERT_EQ(accu(expected_result != result), 0);
    }

    class DiagCovarianceTest : public ::testing::Test{
    protected:
        void SetUp()override {
            A_arma = mat({
                                 {1,0,0,0},
                                 {0,6,0,0},
                                 {0,0,11,0},
                                 {0,0,0,16}});

            B_arma = mat({
                                 {1,0,0,4},
                                 {0,6,0,8},
                                 {9,0,11,0},
                                 {0,14,0,16}}
            );

        }
        mat A_arma;
        mat B_arma;
    };

    TEST_F(DiagCovarianceTest, Constructor){
        DiagCovariance A_diag = DiagCovariance(B_arma);
        ASSERT_EQ(accu(A_arma != A_diag.getFull()), 0);

        DiagCovariance B_diag = DiagCovariance(5);
        ASSERT_EQ(accu(B_diag.getFull() != mat(5, 5, fill::zeros)), 0);

        mat expected_C = mat({
            {1,0,0,0},
            {0,1,0,0},
            {0,0,1,0},
            {0,0,0,1}}
        );
        DiagCovariance C_diag = DiagCovariance(vec(4, fill::ones));
        ASSERT_EQ(accu(C_diag.getFull() != expected_C), 0);
    }

    TEST_F(DiagCovarianceTest, EqualOperator){
        DiagCovariance A_diag;
        DiagCovariance B_diag = DiagCovariance(B_arma);

        A_diag = B_arma;
        ASSERT_EQ(accu(A_arma != A_diag.getFull()), 0);

        A_diag = B_diag;
        ASSERT_EQ(accu(A_arma != A_diag.getFull()), 0);

        A_diag = 5;
        mat expected = mat({
            {5,0,0,0},
            {0,5,0,0},
            {0,0,5,0},
            {0,0,0,5}});
        ASSERT_EQ(accu(expected != A_diag.getFull()), 0);
    }

    TEST_F(DiagCovarianceTest, IncrementOperator){
        DiagCovariance A_diag(A_arma);
        A_diag += mat(4, 4, fill::ones);
        mat result = A_arma;
        result.diag() += 1;
        ASSERT_EQ(accu(result != A_diag.getFull()), 0);

        A_diag += 1;
        result.diag() += 1;
        ASSERT_EQ(accu(A_diag.getFull() != result), 0);
    }

    TEST_F(DiagCovarianceTest, SumOperator){
        mat a_arma(4,4,fill::ones);
        DiagCovariance A_diag(A_arma);
        mat result_l = A_diag + a_arma;
        mat result_r = a_arma + A_diag;
        mat expected_result = a_arma + A_arma;

        ASSERT_EQ(accu(result_l != result_r), 0);
        ASSERT_EQ(accu(result_l != expected_result), 0);
    }

    TEST_F(DiagCovarianceTest, SubtractionOperator){
        mat a_arma(4,4,fill::ones);
        DiagCovariance A_diag(A_arma);
        mat result_l = A_diag - a_arma;
        mat result_r = a_arma - A_diag;
        mat expected_result_l = mat({
                                            {0,-1,-1,-1},
                                            {-1,5,-1,-1},
                                            {-1,-1,10,-1},
                                            {-1,-1,-1,15}
                                    });
        mat expected_result_r = mat({
                                            {0,1,1,1},
                                            {1,-5,1,1},
                                            {1,1,-10,1},
                                            {1,1,1,-15}
                                    });

        ASSERT_EQ(accu(result_l != expected_result_l), 0);
        ASSERT_EQ(accu(result_r != expected_result_r), 0);
    }

    TEST_F(DiagCovarianceTest, ProductOperator){
        DiagCovariance A_diag(B_arma);
        mat a_arma(4,6,fill::ones);
        mat b_arma(6,4,fill::ones);

        mat result_l = A_diag * a_arma;
        mat result_r = b_arma * A_diag;
        vec v_result = A_diag * vec(4, fill::ones);
        vec v_expected({1, 6, 11, 16});
        rowvec w_result = rowvec(4, fill::ones) * A_diag;
        rowvec w_expected({1, 6, 11, 16});


        ASSERT_EQ(accu(result_l != A_arma*a_arma), 0);
        ASSERT_EQ(accu(result_r != b_arma * A_arma), 0);
        ASSERT_EQ( accu(v_result != v_expected), 0);
        ASSERT_EQ(accu(w_result != w_expected), 0);

    }

    TEST_F(DiagCovarianceTest, rankOneUpdate){
        DiagCovariance A_diag(B_arma);
        A_diag.rankOneUpdate(vec(4, fill::ones), 0.1);
        mat result = A_diag.getFull();
        mat expected_result = mat({
            {1.1,0,0,0},
            {0,6.1,0,0},
            {0,0,11.1,0},
            {0,0,0,16.1}
        });

        ASSERT_EQ(accu(expected_result != result), 0);
    }

    class IsoCovarianceTest : public ::testing::Test{
    protected:
        void SetUp()override {
            A_arma = mat({
                                 {9,0,0,0},
                                 {0,9,0,0},
                                 {0,0,9,0},
                                 {0,0,0,9}});

            B_arma = mat({
                                 {3,0,0,4},
                                 {0,6,0,8},
                                 {9,0,11,0},
                                 {0,14,0,16}}
            );

        }
        mat A_arma;
        mat B_arma;
    };

    TEST_F(IsoCovarianceTest, Constructor){
        IsoCovariance A_iso = IsoCovariance(B_arma);
        ASSERT_EQ(accu(A_arma != A_iso.getFull()), 0);

        IsoCovariance B_diag = IsoCovariance(5);
        ASSERT_EQ(accu(B_diag.getFull() != mat(5, 5, fill::zeros)), 0);

        mat expected_C = mat({
                                     {1,0,0,0},
                                     {0,1,0,0},
                                     {0,0,1,0},
                                     {0,0,0,1}}
        );
        IsoCovariance C_iso = IsoCovariance(1, 4);
        ASSERT_EQ(accu(C_iso.getFull() != expected_C), 0);
    }

    TEST_F(IsoCovarianceTest, EqualOperator){
        IsoCovariance A_iso =  IsoCovariance(B_arma);

        ASSERT_EQ(accu(A_arma != A_iso.getFull()), 0);

        A_iso = B_arma;
        ASSERT_EQ(accu(A_arma != A_iso.getFull()), 0);

        A_iso = 5;
        mat expected = mat({
                                   {5,0,0,0},
                                   {0,5,0,0},
                                   {0,0,5,0},
                                   {0,0,0,5}});
        ASSERT_EQ(accu(expected != A_iso.getFull()), 0);
    }

    TEST_F(IsoCovarianceTest, IncrementOperator){
        IsoCovariance A_iso(A_arma);
        A_iso += mat(4, 4, fill::ones);
        mat result = A_arma;
        result.diag() += 1;
        ASSERT_EQ(accu(result != A_iso.getFull()), 0);

        A_iso += 1;
        result.diag() += 1;
        ASSERT_EQ(accu(A_iso.getFull() != result), 0);
    }

    TEST_F(IsoCovarianceTest, SumOperator){
        mat a_arma(4,4,fill::ones);
        IsoCovariance A_iso(A_arma);
        mat result_l = A_iso + a_arma;
        mat result_r = a_arma + A_iso;
        mat expected_result = a_arma + A_arma;

        ASSERT_EQ(accu(result_l != result_r), 0);
        ASSERT_EQ(accu(result_l != expected_result), 0);
    }

    TEST_F(IsoCovarianceTest, SubtractionOperator){
        mat a_arma(4,4,fill::ones);
        IsoCovariance A_iso(A_arma);
        mat result_l = A_iso - a_arma;
        mat result_r = a_arma - A_iso;
        mat expected_result_l = mat({
                                            {8,-1,-1,-1},
                                            {-1,8,-1,-1},
                                            {-1,-1,8,-1},
                                            {-1,-1,-1,8}
                                    });
        mat expected_result_r = mat({
                                            {-8,1,1,1},
                                            {1,-8,1,1},
                                            {1,1,-8,1},
                                            {1,1,1,-8}
                                    });

        ASSERT_EQ(accu(result_l != expected_result_l), 0);
        ASSERT_EQ(accu(result_r != expected_result_r), 0);
    }

    TEST_F(IsoCovarianceTest, ProductOperator){
        IsoCovariance A_iso(B_arma);
        mat a_arma(4,6,fill::ones);
        mat b_arma(6,4,fill::ones);

        mat result_l = A_iso * a_arma;
        mat result_r = b_arma * A_iso;
        vec v_result = A_iso * vec(4, fill::ones);
        vec v_expected({9, 9, 9, 9});
        rowvec w_result = rowvec(4, fill::ones) * A_iso;
        rowvec w_expected({9, 9, 9, 9});


        ASSERT_EQ(accu(result_l != A_arma*a_arma), 0);
        ASSERT_EQ(accu(result_r != b_arma * A_arma), 0);
        ASSERT_EQ( accu(v_result != v_expected), 0);
        ASSERT_EQ(accu(w_result != w_expected), 0);

    }

    TEST_F(IsoCovarianceTest, rankOneUpdate){
        IsoCovariance A_iso(B_arma);
        A_iso.rankOneUpdate(vec(4, fill::ones), 0.1);
        mat result = A_iso.getFull();
        mat expected_result = mat({
                                          {9.1,0,0,0},
                                          {0,9.1,0,0},
                                          {0,0,9.1,0},
                                          {0,0,0,9.1}
                                  });

        ASSERT_EQ(accu(expected_result != result), 0);
    }

}

