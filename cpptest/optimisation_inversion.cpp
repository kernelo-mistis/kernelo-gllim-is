#include <armadillo>
#include <stdexcept>
#include "matplotlibcpp.h"

#include "../src/learningModel/gllim/GLLiMLearning.h"
#include "../src/learningModel/covariances/Icovariance.h"
#include "../src/learningModel/gllim/GLLiMParameters.h"


namespace plt = matplotlibcpp;
using namespace learningModel;
using namespace arma;


template<typename T, typename U>
GLLiMParameters<T,U> setUpGllimParameters(GLLiMParameters<T,U> myParams, int L, int D, int K) {

    arma_rng::set_seed_random();
    mat T_L = trimatl(mat(L, L, fill::randu));
    mat S_L = T_L * T_L.t();
    S_L.diag() += 1;
    arma_rng::set_seed_random();

    mat T_D = trimatl(mat(D, D, fill::randu));
    mat S_D = T_D * T_D.t();
    S_D.diag() += 1;
    arma_rng::set_seed_random();

    // GLLiMParameters<DiagCovariance,DiagCovariance> myParams = GLLiMParameters<DiagCovariance,DiagCovariance>(D,L,K);
    myParams.Pi = normalise(vec(K, fill::randu), 1);

    for(unsigned p=0; p < K ; p++ ){

        myParams.Gamma[p] = T(S_L);
        myParams.Sigma[p] = U(S_D);

        myParams.A.slice(p) = mat(D,L, fill::randu);
        arma_rng::set_seed_random();
        myParams.B.col(p) = vec(D, fill::randu);
        arma_rng::set_seed_random();
        myParams.C.col(p) = vec(L, fill::randu);
        arma_rng::set_seed_random();
    }

    return myParams;
}


template<typename T, typename U>
GLLiMParameters<FullCovariance,FullCovariance> inverse(GLLiMParameters<T,U> &gllim_direct) {

    GLLiMParameters<FullCovariance, FullCovariance> gllim_inv(gllim_direct.L, gllim_direct.D, gllim_direct.K);

    for(unsigned k=0; k<gllim_direct.K; k++){
        if(gllim_direct.Pi(k) != 0){
            gllim_inv.Pi(k) = gllim_direct.Pi(k);
            U sigma_inv = U(gllim_direct.Sigma[k].inv());
            T gamma_inv = T(gllim_direct.Gamma[k].inv());
            gllim_inv.C.col(k) = gllim_direct.A.slice(k) * gllim_direct.C.col(k) + gllim_direct.B.col(k);
            gllim_inv.Gamma[k] = FullCovariance(gllim_direct.Sigma[k] + gllim_direct.A.slice(k) * gllim_direct.Gamma[k] * gllim_direct.A.slice(k).t());
            gllim_inv.Sigma[k] = FullCovariance(gamma_inv + mat(gllim_direct.A.slice(k).t()) * sigma_inv * mat(gllim_direct.A.slice(k))).inv();
            gllim_inv.A.slice(k) = gllim_inv.Sigma[k] * mat(gllim_direct.A.slice(k).t()) * sigma_inv;
            gllim_inv.B.col(k) = gllim_inv.Sigma[k] * vec(gamma_inv * vec(gllim_direct.C.col(k)) - mat(gllim_direct.A.slice(k).t()) * sigma_inv * vec(gllim_direct.B.col(k)));
        }
    }
    return gllim_inv;
}


template<typename T, typename U>
gmm_full logDensity_naive(GLLiMParameters<T,U> gllim, const vec &x) {
    // Naive implementation of log-density: cov.inv()

    gmm_full model;
    double log_det_gamma;
    vec x_u;
    vec weights(gllim.K,fill::zeros);

    for(unsigned k=0; k<gllim.K; k++){
        if(gllim.Pi(k) == 0){
            weights(k) = -datum::inf;
        }else{
            log_det_gamma = gllim.Gamma[k].log_det();
            x_u = x - gllim.C.col(k);
            if(log_det_gamma != -datum::inf){
                weights(k) = log(gllim.Pi(k)) - 0.5 * (gllim.L * log(2* datum::pi) + log_det_gamma + dot((rowvec(x_u.t()) * gllim.Gamma[k].inv()).t(), x_u));
            }
        }
    }

    double result = 0;
    double max = weights.max();
    if(max != -datum::inf){
        for(unsigned k=0; k<gllim.K; k++){
            result += exp(weights(k) - max);
        }
        result = log(result) + max;
    }
    if(result != -datum::inf){
        weights = exp(weights - result);
    }

    // means
    mat means(gllim.D,gllim.K);
    for(unsigned k=0; k<gllim.K; k++){
        means.col(k) = gllim.A.slice(k) * x + gllim.B.col(k);
    }

    // covariances
    cube covariances(gllim.D,gllim.D,gllim.K);
    for(unsigned k=0; k<gllim.K; k++){
        covariances.slice(k) = gllim.Sigma[k].getFull();
    }

    model.set_params(means,covariances,weights.t());
    return model;
}


template<typename T, typename U>
gmm_full logDensity_fast(GLLiMParameters<T,U> gllim, const vec &x) {
    // Fast and general implementation of log-density. Works with Full covariance matrix

    gmm_full model;
    vec weights(gllim.K,fill::zeros);

    for(unsigned k=0; k<gllim.K; k++){
        if(gllim.Pi(k) == 0){
            weights(k) = -datum::inf;
        }else{
            mat Gamma_k = gllim.Gamma[k].getFull(); // je pense que cela prend du temps surtout lorsque D est grand.
            weights(k) = log(gllim.Pi(k)) + Helpers::mvnrm_arma_fast_chol(rowvec(x.t()), rowvec(gllim.C.col(k).t()), Gamma_k);
        }
    }

    double result = 0;
    double max = weights.max();
    if(max != -datum::inf){
        for(unsigned k=0; k<gllim.K; k++){
            result += exp(weights(k) - max);
        }
        result = log(result) + max;
    }
    if(result != -datum::inf){
        weights = exp(weights - result);
    }

    // means
    mat means(gllim.D,gllim.K);
    for(unsigned k=0; k<gllim.K; k++){
        means.col(k) = gllim.A.slice(k) * x + gllim.B.col(k);
    }

    // covariances
    cube covariances(gllim.D,gllim.D,gllim.K);
    for(unsigned k=0; k<gllim.K; k++){
        covariances.slice(k) = gllim.Sigma[k].getFull();
    }

    model.set_params(means,covariances,weights.t());
    return model;
}


template<typename T, typename U>
gmm_full logDensity_woodbury_basic(GLLiMParameters<T,U> gllim, GLLiMParameters<FullCovariance, FullCovariance> gllim_inv, const vec &x) {
    // Optimzed logpdf computation with Woodbury formula and matrix determiannt lemma. Useful when T=Full and Sigma=Diag.

    gmm_full model;
    double log_det_gamma;
    double quadratic;
    vec weights(gllim.K,fill::zeros);
    vec x_u(gllim.D);
    vec w(gllim.D);
    vec z(gllim.L);
    FullCovariance M(gllim.L);

    using arma::uword;
    uword const xdim = x.n_rows;
    double constants = -(double)xdim/2.0 * log(2* datum::pi);
    mat rooti(gllim.L, gllim.L);

    for(unsigned k=0; k<gllim.K; k++){
        if(gllim.Pi(k) == 0){
            weights(k) = -datum::inf;
        }else{
            x_u = x - (gllim.A.slice(k) * gllim.C.col(k) + gllim.B.col(k));
            w = gllim.Sigma[k].inv() * x_u;
            z = gllim.A.slice(k).t() * w;
            M = gllim.Gamma[k].inv() + gllim.A.slice(k).t() * (gllim.Sigma[k].inv() * gllim.A.slice(k));

            log_det_gamma = M.log_det() + gllim.Gamma[k].log_det() + gllim.Sigma[k].log_det(); // log_det() is the armadillo function and return std::complex<double> whereas .log_det() is the ICovariances method and return double
            quadratic = dot(x_u.t(),w) - dot((rowvec(z.t()) * M.inv()).t(), z);

            weights(k) = log(gllim.Pi(k)) + constants - 0.5 * (log_det_gamma + quadratic);
        }
    }

    double result = 0;
    double max = weights.max();
    if(max != -datum::inf){
        for(unsigned k=0; k<gllim_inv.K; k++){
            result += exp(weights(k) - max);
        }
        result = log(result) + max;
    }
    if(result != -datum::inf){
        weights = exp(weights - result);
    }

    // means
    mat means(gllim_inv.D,gllim_inv.K);
    for(unsigned k=0; k<gllim_inv.K; k++){
        means.col(k) = gllim_inv.A.slice(k) * x + gllim_inv.B.col(k);
    }

    // covariances
    cube covariances(gllim_inv.D,gllim_inv.D,gllim_inv.K);
    for(unsigned k=0; k<gllim_inv.K; k++){
        covariances.slice(k) = gllim_inv.Sigma[k].getFull();
    }

    model.set_params(means,covariances,weights.t());
    return model;
}


template<typename T, typename U>
gmm_full logDensity_woodbury_opti(GLLiMParameters<T,U> gllim, GLLiMParameters<FullCovariance, FullCovariance> gllim_inv, const vec &x) {
    // Optimzed logpdf computation with Woodbury formula and matrix determiannt lemma. Useful when T=Full and Sigma=Diag.

    gmm_full model;
    double log_det_gamma;
    double quadratic;
    vec weights(gllim.K,fill::zeros);
    vec x_u(gllim.D);
    vec w(gllim.D);
    rowvec z(gllim.L);
    mat M(gllim.L, gllim.L);

    using arma::uword;
    uword const xdim = x.n_rows;
    double constants = -(double)xdim/2.0 * log(2* datum::pi);
    mat rooti(gllim.L, gllim.L);

    for(unsigned k=0; k<gllim.K; k++){
        if(gllim.Pi(k) == 0){
            weights(k) = -datum::inf;
        }else{
            x_u = x - (gllim.A.slice(k) * gllim.C.col(k) + gllim.B.col(k));
            w = gllim.Sigma[k].inv() * x_u;
            z = (gllim.A.slice(k).t() * w).t();
            M = gllim.Gamma[k].inv() + gllim.A.slice(k).t() * (gllim.Sigma[k].inv() * gllim.A.slice(k));

            log_det_gamma = real(log_det(M)) + gllim.Gamma[k].log_det() + gllim.Sigma[k].log_det(); // log_det() is the armadillo function and .log_det() is the ICovariances method
            rooti = arma::inv(arma::trimatu(Helpers::safe_cholesky(M)));
            Helpers::inplace_tri_mat_mult(z, rooti);
            quadratic = dot(x_u.t(),w) - dot(z,z);

            weights(k) = log(gllim.Pi(k)) + constants - 0.5 * (log_det_gamma + quadratic);
        }
    }

    double result = 0;
    double max = weights.max();
    if(max != -datum::inf){
        for(unsigned k=0; k<gllim_inv.K; k++){
            result += exp(weights(k) - max);
        }
        result = log(result) + max;
    }
    if(result != -datum::inf){
        weights = exp(weights - result);
    }

    // means
    mat means(gllim_inv.D,gllim_inv.K);
    for(unsigned k=0; k<gllim_inv.K; k++){
        means.col(k) = gllim_inv.A.slice(k) * x + gllim_inv.B.col(k);
    }

    // covariances
    cube covariances(gllim_inv.D,gllim_inv.D,gllim_inv.K);
    for(unsigned k=0; k<gllim_inv.K; k++){
        covariances.slice(k) = gllim_inv.Sigma[k].getFull();
    }

    model.set_params(means,covariances,weights.t());
    return model;
}


int main(){
    std::cout << "Testing log-density computation performance" << std::endl;

    // Declare global constants and variables
    int L = 10;
    int D;
    std::vector<int> D_list = {10, 50, 100, 200, 300, 500, 700, 1000};
    int n_test = 10;
    int K = 50;

    gmm_full model_naive(L,K);
    gmm_full model_fast(L,K);
    gmm_full model_woodbury_basic(L,K);
    gmm_full model_woodbury_opti(L,K);

    auto start = std::chrono::high_resolution_clock::now();
    auto end = std::chrono::high_resolution_clock::now();

    std::vector<double> duration_inverse(D_list.size());
    std::vector<double> duration_naive(D_list.size());
    std::vector<double> duration_fast(D_list.size());
    std::vector<double> duration_woodbury_basic(D_list.size());
    std::vector<double> duration_woodbury_opti(D_list.size());


    for(unsigned i=0; i < D_list.size() ; i++ ){
        D = D_list[i];

        // Loop sur les type de matrices (Diag, Full, Iso). Faire par cas
        for(unsigned n=0; n < n_test ; n++ ){

            // Set up test
            GLLiMParameters<FullCovariance,DiagCovariance> myParams(D,L,K);
            GLLiMParameters<FullCovariance,DiagCovariance> gllim_direct = setUpGllimParameters(myParams, L, D, K);
            vec x(D, fill::randu);
            GLLiMParameters<FullCovariance,FullCovariance> gllim_inv(L,D,K);

            start = std::chrono::high_resolution_clock::now();
            gllim_inv = inverse(gllim_direct);
            end = std::chrono::high_resolution_clock::now();
            duration_inverse[i] = duration_inverse[i] + std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

            start = std::chrono::high_resolution_clock::now();
            gllim_inv = inverse(gllim_direct);
            model_naive = logDensity_naive(gllim_inv, x);
            end = std::chrono::high_resolution_clock::now();
            duration_naive[i] = duration_naive[i] + std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

            start = std::chrono::high_resolution_clock::now();
            gllim_inv = inverse(gllim_direct);
            model_fast = logDensity_fast(gllim_inv, x);
            end = std::chrono::high_resolution_clock::now();
            duration_fast[i] = duration_fast[i] + std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

            start = std::chrono::high_resolution_clock::now();
            gllim_inv = inverse(gllim_direct);
            model_woodbury_basic = logDensity_woodbury_basic(gllim_direct, gllim_inv, x);
            end = std::chrono::high_resolution_clock::now();
            duration_woodbury_basic[i] = duration_woodbury_basic[i] + std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

            start = std::chrono::high_resolution_clock::now();
            gllim_inv = inverse(gllim_direct);
            model_woodbury_opti = logDensity_woodbury_opti(gllim_direct, gllim_inv, x);
            end = std::chrono::high_resolution_clock::now();
            duration_woodbury_opti[i] = duration_woodbury_opti[i] + std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            
            // std::cout << model_naive.hefts << std::endl;
            // std::cout << model_fast.hefts << std::endl;
            // std::cout << model_woodbury_basic.hefts << std::endl;
            // std::cout << model_woodbury_opti.hefts << std::endl;

            // Check that weights are equals among all methods
            if(!approx_equal(model_naive.hefts, model_fast.hefts, "absdiff", 1e-8)){
                throw std::invalid_argument( "Method error : Fast general => weights" );
            }
            if(!approx_equal(model_naive.hefts, model_woodbury_basic.hefts, "absdiff", 1e-8)){
                throw std::invalid_argument( "Method error : woodbury_basic => weights" );
            }
            if(!approx_equal(model_naive.hefts, model_woodbury_opti.hefts, "absdiff", 1e-8)){
                throw std::invalid_argument( "Method error : woodbury_opti => weights" );
            }
            // Check that means are equals among all methods
            if(!approx_equal(model_naive.means, model_fast.means, "absdiff", 1e-8)){
                throw std::invalid_argument( "Method error : Fast general => means" );
            }
            if(!approx_equal(model_naive.means, model_woodbury_basic.means, "absdiff", 1e-8)){
                throw std::invalid_argument( "Method error : woodbury_basic => means" );
            }
            if(!approx_equal(model_naive.means, model_woodbury_opti.means, "absdiff", 1e-8)){
                throw std::invalid_argument( "Method error : woodbury_opti => means" );
            }
            // Check that weights are equals among all methods
            if(!approx_equal(model_naive.fcovs, model_fast.fcovs, "absdiff", 1e-8)){
                throw std::invalid_argument( "Method error : Fast general => covs" );
            }
            if(!approx_equal(model_naive.fcovs, model_woodbury_basic.fcovs, "absdiff", 1e-8)){
                throw std::invalid_argument( "Method error : woodbury_basic => covs" );
            }
            if(!approx_equal(model_naive.fcovs, model_woodbury_opti.fcovs, "absdiff", 1e-8)){
                throw std::invalid_argument( "Method error : woodbury_opti => covs" );
            }

        }

        // Print computation time mean
        duration_inverse[i] = duration_inverse[i] / n_test;
        duration_naive[i] = duration_naive[i] / n_test;
        duration_fast[i] = duration_fast[i] / n_test;
        duration_woodbury_basic[i] = duration_woodbury_basic[i] / n_test;
        duration_woodbury_opti[i] = duration_woodbury_opti[i] / n_test;
        // std::cout << "Inverse gllim (ms)" << std::to_string(duration_inverse) << std::endl;
        // std::cout << "Naive time (ms)" << std::to_string(duration_naive) << std::endl;
        // std::cout << "Fast time (ms)" << std::to_string(duration_fast) << std::endl;
        // std::cout << "Woodbury alamain time (ms)" << std::to_string(duration_woodbury_basic) << std::endl;
        // std::cout << "Woodbury trimatu time (ms)" << std::to_string(duration_woodbury_opti) << std::endl;
    }

    plt::figure();
    
    plt::named_plot("inverse gllim", D_list, duration_inverse, "k--");
    plt::named_plot("Naive", D_list, duration_naive, "r");
    plt::named_plot("Fast general", D_list, duration_fast, "b");
    plt::named_plot("Woodbury", D_list, duration_woodbury_basic, "y");
    plt::named_plot("Woodbury fast", D_list, duration_woodbury_opti, "g");

    plt::ylabel("Computation time for K gaussians (ms)");
    plt::xlabel("D dimension");
    
    plt::title("Computation time comparison\n(L=10,K=50)\n<FullCovariance,DiagCovariance>");
    plt::legend();
    plt::show();

    return 0;
}

