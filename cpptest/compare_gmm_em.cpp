#include <armadillo>
#include <stdexcept>
#include <tuple>
#include "matplotlibcpp.h"

#include "../src/learningModel/gllim/GLLiMLearning.h"
#include "../src/learningModel/estimators/GmmEstimator.h"
#include "../src/learningModel/estimators/EmEstimator.h"
#include "../src/learningModel/configs/LearningConfig.h"
#include "../src/learningModel/covariances/Icovariance.h"
#include "../src/learningModel/gllim/GLLiMParameters.h"

// Initializer imports
#include "../src/learningModel/initializers/MultInitializer.h"
#include "../src/learningModel/initializers/Initializers.h"
#include "../src/learningModel/configs/InitConfig.h"
#include "../src/dataGeneration/GeneratorStrategy.h"
#include "../src/dataGeneration/RandomGenerator.h"

// logging imports
#include "../src/logging/Logger.h"
#include "../src/logging/pyhelper.hpp"
#include "../src/functionalModel/ExternalModel/pyhelper.hpp"

namespace plt = matplotlibcpp;
using namespace learningModel;
using namespace arma;

std::tuple<mat, mat, std::shared_ptr<GLLiMParameters<FullCovariance, FullCovariance>>> setUpGllimParameters(int L, int D, int K, int N)
{
    std::shared_ptr<GLLiMParameters<FullCovariance, FullCovariance>> initial_theta;
    mat X, Y;

    // Fixer A et B
    arma_rng::set_seed(10000);
    mat A(D, L, fill::randu);
    arma_rng::set_seed(11000);
    vec B(D, fill::randu);

    // Fixer sigma
    mat Sigma(N, D, fill::randn);
    Sigma *= sqrt(0.01);

    // Fixer C, Gamma et Pi
    arma_rng::set_seed(11100);
    vec Pi = normalise(vec(K, fill::randu), 1);

    arma_rng::set_seed(11110);
    mat C(L, K, fill::randu);

    cube Gamma(L, L, K);

    for (unsigned p = 0; p < K; p++)
    {
        arma_rng::set_seed_random();
        mat T_L = trimatl(mat(L, L, fill::randu));
        Gamma.slice(p) = T_L * T_L.t();
        Gamma.slice(p).diag() += 1;
    }

    // init gmm
    gmm_full model;
    model.set_params(C, Gamma, Pi.t());

    // sample X
    X = model.generate(N);

    // Calculer Y
    Y = mat(A * X);
    for (unsigned n = 0; n < N; n++)
    {
        for (unsigned d = 0; d < D; d++)
        {
            Y(d, n) += B(d) + Sigma(n, d);
        }
    }

    mat sig(D, D, fill::zeros);
    sig.diag() += 0.01;

    initial_theta = std::make_shared<GLLiMParameters<FullCovariance, FullCovariance>>(D, L, K);
    initial_theta->Pi = normalise(vec(K, fill::randu), 1);
    for (unsigned k = 0; k < K; k++)
    {
        initial_theta->Sigma[k] = FullCovariance(sig);
        initial_theta->Gamma[k] = FullCovariance(Gamma.slice(k));
        initial_theta->A.slice(k) = A;
        initial_theta->B.col(k) = B;
        initial_theta->C.col(k) = C.col(k);
    }

    return std::make_tuple(X, Y, initial_theta);
}


std::shared_ptr<GLLiMParameters<FullCovariance, FullCovariance>> doInitialisation(mat X, mat Y, unsigned K)
{
    std::shared_ptr<GLLiMParameters<FullCovariance, FullCovariance>> initial_theta;
    std::shared_ptr<MultInitConfig> myConfig (
            new MultInitConfig(
                    123456789,
                    5,
                    10,
                    std::make_shared<GMMLearningConfig>(GMMLearningConfig(5,10,1e-8)),
                    std::make_shared<EMLearningConfig>(EMLearningConfig(10,0,1e-08))));

    MultInitializer<FullCovariance,FullCovariance> initializer(myConfig);

    initial_theta = initializer.execute(X, Y, K);
    return initial_theta;
}


int main()
{
    std::cout << "Comparing GMM algo (armadillo) and EM algo (kernelo)" << std::endl;

    // Declare global constants and variables
    int L = 20;
    int D = 100;
    int K = 1;
    std::vector<int> K_list = {1,5,10,20,50};
    int N = 5000;

    auto start = std::chrono::high_resolution_clock::now();
    auto end = std::chrono::high_resolution_clock::now();
    std::vector<double> duration_gmm(K_list.size());
    std::vector<double> duration_em(K_list.size());

    for(unsigned i=0; i < K_list.size() ; i++ ){
        K = K_list[i];
        std::cout << "Test for K=" << std::to_string(K) << std::endl;

        std::shared_ptr<GMMLearningConfig> gmm_config = std::make_shared<GMMLearningConfig>(0, 100, 1e-8);
        GmmEstimator gmm_estimator = GmmEstimator(gmm_config);

        std::shared_ptr<EMLearningConfig> em_config = std::make_shared<EMLearningConfig>(100, 1e-5, 1e-8);
        EmEstimator<FullCovariance, FullCovariance> em_estimator = EmEstimator<FullCovariance, FullCovariance>(em_config);

        std::shared_ptr<GLLiMParameters<FullCovariance, FullCovariance>> gmm_theta;
        std::shared_ptr<GLLiMParameters<FullCovariance, FullCovariance>> em_theta;
        mat X, Y;

        std::tie(X, Y, gmm_theta) = setUpGllimParameters(L, D, K, N);

        // std::cout << "quick COMPARING initial theta" << std::endl;
        // std::cout << gmm_theta->Pi << std::endl;
        // gmm_theta->A.slice(0).print("A:");
        // do initialisation to improve initial_theta quality
        gmm_theta = doInitialisation(X.t(), Y.t(), K);
        // std::cout << gmm_theta->Pi << std::endl;
        // gmm_theta->A.slice(0).print("A:");

        em_theta = std::make_shared<GLLiMParameters<FullCovariance, FullCovariance>>(D, L, K);
        for (unsigned k = 0; k < K; k++)
        {
            em_theta->Pi[k] = gmm_theta->Pi[k];
            em_theta->Sigma[k] = gmm_theta->Sigma[k];
            em_theta->Gamma[k] = gmm_theta->Gamma[k];
            em_theta->A.slice(k) = gmm_theta->A.slice(k);
            em_theta->B.col(k) = gmm_theta->B.col(k);
            em_theta->C.col(k) = gmm_theta->C.col(k);
        };

        std::cout << "Train with GMM algo" << std::endl;
        start = std::chrono::high_resolution_clock::now();
        gmm_estimator.execute(X.t(), Y.t(), gmm_theta);
        end = std::chrono::high_resolution_clock::now();
        duration_gmm[i] = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        std::cout << "Time : " << duration_gmm[i] << " microsec" << std::endl;

        std::cout << "Train with EM algo" << std::endl;
        start = std::chrono::high_resolution_clock::now();
        em_estimator.execute(X.t(), Y.t(), em_theta);
        end = std::chrono::high_resolution_clock::now();
        duration_em[i] = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        std::cout << "Time : " << duration_em[i] << " microsec" << std::endl;

        // // Check if theta are the same
        // if(!approx_equal(gmm_theta->Pi, em_theta->Pi, "reldiff", 1e-3)){
        //     std::cout << gmm_theta->Pi << std::endl;
        //     std::cout << em_theta->Pi << std::endl;
        //     throw std::invalid_argument( "Step K=" + std::to_string(K) + "\nPi are not equal" );
        // }
        // if(!approx_equal(gmm_theta->A, em_theta->A, "reldiff", 1e-3)){
        //     gmm_theta->A.print("A:");
        //     em_theta->A.print("A:");
        //     throw std::invalid_argument( "Step K=" + std::to_string(K) + "\nA are not equal" );
        // }
        // if(!approx_equal(gmm_theta->B, em_theta->B, "reldiff", 1e-3)){
        //     throw std::invalid_argument( "Step K=" + std::to_string(K) + "\nB are not equal" );
        //     gmm_theta->B.print("B:");
        //     em_theta->B.print("B:");
        // }
        // if(!approx_equal(gmm_theta->C, em_theta->C, "reldiff", 1e-3)){
        //     gmm_theta->C.print("C:");
        //     em_theta->C.print("C:");
        //     throw std::invalid_argument( "Step K=" + std::to_string(K) + "\nC are not equal" );
        // }
        // for (unsigned k = 0; k < K; k++)
        // {
        //     if(!approx_equal(gmm_theta->Gamma[k].getFull(), em_theta->Gamma[k].getFull(), "reldiff", 1e-3)){
        //         gmm_theta->Gamma[k].print();
        //         em_theta->Gamma[k].print();
        //         throw std::invalid_argument( "Step K=" + std::to_string(K) + "\nGamma[" + std::to_string(k) + "] are not equal" );
        //     }
        //     if(!approx_equal(gmm_theta->Sigma[k].getFull(), em_theta->Sigma[k].getFull(), "reldiff", 1e-3)){
        //         gmm_theta->Sigma[k].print();
        //         em_theta->Sigma[k].print();
        //         throw std::invalid_argument( "Step K=" + std::to_string(K) + "\nSigma[" + std::to_string(k) + "] are not equal" );
        //     }
        // }
    }
    
    plt::figure();
    
    plt::named_plot("GMM", K_list, duration_gmm, "kx");
    plt::named_plot("EM", K_list, duration_em, "rx");

    plt::ylabel("Computation time (microsec)");
    plt::xlabel("K dimension");
    
    plt::title("Computation time comparison GMM/EM");
    plt::legend();
    plt::show();

    return 0;
}
