cmake_minimum_required(VERSION 3.15)

project(Google_tests)

add_executable(importanceSamplingTest ImisTest.cpp)
target_link_libraries(importanceSamplingTest gtest_main prediction learningModel dataGeneration functionalModel ${ARMADILLO_LIBRARIES} ${PYTHON_LIBRARIES})
# Here we specify all project librairies because ImisTest needs it