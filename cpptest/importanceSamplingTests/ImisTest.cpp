#include <gtest/gtest.h>
#include <armadillo>
#include "../../src/importanceSampling/creators.h"
#include "../../src/importanceSampling/Imis.h"
#include "../../src/importanceSampling/ImportanceSampler.h"
#include "../../src/importanceSampling/proposition/GaussianMixtureProposition.h"
#include "../../src/importanceSampling/proposition/ISProposition.h"
#include "../../src/functionalModel/TestModel/TestModel.h"
#include "../../src/dataGeneration/creators.h"

namespace importanceSampling {

    class ImisTest : public ::testing::Test{
        protected:
            void SetUp() override {
                // For each test defined with TEST_F(), GoogleTest will create a fresh test fixture at runtime, immediately initialize it via SetUp()
                // Here we define basic elements for tests

                // Test model
                testModel = std::shared_ptr<Functional::TestModel>((new TestModel()));
                L = testModel->get_L_dimension();
                D = testModel->get_D_dimension();

                // Stat model
                std::shared_ptr<Functional::FunctionalModel> functionalModel = std::shared_ptr<Functional::FunctionalModel>(testModel);
                BasicDependentGaussianStatModel = std::shared_ptr<DataGeneration::StatModel>(DataGeneration::DependentGaussianStatModelConfig("sobol", functionalModel, 50 , 12345).create());
                // BasicGaussianStatModel = std::shared_ptr<DataGeneration::StatModel>(DataGeneration::GaussianStatModelConfig("sobol", functionalModel, 0.01, 9, 12345).create());
                // Note: In the IMIS process only F(x) is evaluated within density_X_Y() function. generator type and seed have no impact here
                // We should test with the other statModel: GaussianStatModel

                // Target law
                BasicTarget.setTarget(BasicDependentGaussianStatModel);

                // Proposition law
                vec gmm_weights(1, arma::fill::ones);
                mat gmm_means(L, 1);
                gmm_means.col(0) = vec("0.2 0.4 0.6 0.8");
                cube gmm_covs(L, L, 1);
                gmm_covs.slice(0).eye(L,L);
                gmm_covs *= 0.01;
                BasicProposition = std::shared_ptr<ISProposition>(new GaussianMixtureProposition (gmm_weights, gmm_means, gmm_covs));

                // Basic observations
                x_obs = randu(L);
                rowvec y_obs_rowvec(D);
                testModel->F(x_obs.t(), y_obs_rowvec);
                y_obs = y_obs_rowvec.t();
                y_cov = vec(D, arma::fill::ones);
                y_cov *= 0.001;

            }      

        ISTarget BasicTarget;
        std::shared_ptr<Functional::TestModel> testModel;
        std::shared_ptr<DataGeneration::StatModel> BasicDependentGaussianStatModel, BasicGaussianStatModel;
        std::shared_ptr<ISProposition> BasicProposition;
        unsigned L;
        unsigned D;
        vec x_obs, y_obs, y_cov;
        unsigned N_try = 100;
        unsigned N_experiences = 5;
        
    };

    TEST_F(ImisTest, SumWeightsEqualsOne){
        unsigned N_0 = 100;
        ImportanceSampler is_sampler(N_0, std::make_shared<ISTarget>(BasicTarget));
        mat is_samples(L, N_0);
        vec is_weights(N_0), is_target_log_densities(N_0), is_proposition_log_densities(N_0);

        is_sampler.execute(is_samples, is_weights, is_target_log_densities, is_proposition_log_densities, y_obs, y_cov, BasicProposition);
        ASSERT_EQ(accu(is_weights), 1);

    };

    TEST_F(ImisTest, ImisWithNoIterationEqualsIS){
        unsigned N_0 = 100;
        unsigned B = 0;
        unsigned J = 0; // No iteration
        ImportanceSampler is_sampler(N_0, std::make_shared<ISTarget>(BasicTarget));
        Imis imis_sampler(N_0,B,J, std::make_shared<ISTarget>(BasicTarget));
        arma_rng::set_seed(12345);
        ISResult is_result = is_sampler.execute(BasicProposition, y_obs, y_cov);
        arma_rng::set_seed(12345);
        ISResult imis_result = imis_sampler.execute(BasicProposition, y_obs, y_cov);

        ASSERT_EQ(is_result.mean.size(), imis_result.mean.size()) << "Vectors is_mean and imis_mean are of unequal length";
        for (int i = 0; i < is_result.mean.size(); ++i) {
            EXPECT_EQ(is_result.mean[i], imis_result.mean[i]) << "Vectors is_mean and imis_mean differ at index " << i;
        }
        ASSERT_EQ(is_result.covariance.size(), imis_result.covariance.size()) << "Vectors is_covariance and imis_covariance are of unequal length";
        for (int i = 0; i < is_result.covariance.size(); ++i) {
            EXPECT_EQ(is_result.covariance[i], imis_result.covariance[i]) << "Vectors is_covariance and imis_covariance differ at index " << i;
        }
    };

    TEST_F(ImisTest, ISPerformance){

        for(unsigned r=0; r<N_experiences; r++){

            unsigned N_samples = 100*(1+5*r);
            ImportanceSampler is_sampler(N_samples, std::make_shared<ISTarget>(BasicTarget));

            vec x_obs = randu(L);
            rowvec y_obs_rowvec(D);
            testModel->F(x_obs.t(), y_obs_rowvec);
            vec y_obs = y_obs_rowvec.t();
            vec y_cov(D, arma::fill::ones);
            y_cov *= 0.001;

            double error_on_x_is = 0;
            double error_reconstruction_is = 0;

            for(unsigned n=0; n<N_try; n++){
                
                ISResult is_result = is_sampler.execute(BasicProposition, y_obs, y_cov);

                vec x_pred_is = is_result.mean;
                rowvec y_pred_is(D);
                testModel->F(x_pred_is.t(), y_pred_is);
                error_on_x_is += norm(x_pred_is - x_obs, "inf");
                error_reconstruction_is += norm(y_pred_is.t()-y_obs, 2)/norm(y_obs, 2);
            }

            error_on_x_is /= N_try;
            error_reconstruction_is /= N_try;
            EXPECT_TRUE((error_on_x_is >= 0) && (error_on_x_is < 1)) << "Error on x is too high for IS at experience " << r;
            EXPECT_TRUE((error_reconstruction_is >= 0) && (error_reconstruction_is < 1)) << "Reconstruction error is too high for IS at experience " << r;
        }

    };

    TEST_F(ImisTest, IMISPerformance){
        // On a 20 expériences 100 fois le test
        // definir les différentes fonctionnelle de test: il faudrait les mettre dans un fichier du dossier cpptest (pas dans src/)
        // Learn GLLiM avec cov = 0.001^2 et K=50
        // Generate observations (x_obs, y_obs=F(x_obs)) with sigma = y_obs/50
        // Apply IS, IMIS1, IMIS1

        for(unsigned r=0; r<N_experiences; r++){

            unsigned N_samples = 100*(1+5*r);
            unsigned N_0 = N_samples/10;
            unsigned B = N_samples/20;
            unsigned J = 18;

            Imis imis_sampler(N_0,B,J, std::make_shared<ISTarget>(BasicTarget));

            vec x_obs = randu(L);
            rowvec y_obs_rowvec(D);
            testModel->F(x_obs.t(), y_obs_rowvec);
            vec y_obs = y_obs_rowvec.t();
            vec y_cov(D, arma::fill::ones);
            y_cov *= 0.001;

            double error_on_x_imis = 0;
            double error_reconstruction_imis = 0;

            for(unsigned n=0; n<N_try; n++){
                
                ISResult imis_result = imis_sampler.execute(BasicProposition, y_obs, y_cov);

                vec x_pred_imis = imis_result.mean;
                rowvec y_pred_imis(D);
                testModel->F(x_pred_imis.t(), y_pred_imis);
                error_on_x_imis += norm(x_pred_imis - x_obs, "inf");
                error_reconstruction_imis += norm(y_pred_imis.t()-y_obs, 2)/norm(y_obs, 2);
            }
                
            error_on_x_imis /= N_try;
            error_reconstruction_imis /= N_try;
            EXPECT_TRUE((error_on_x_imis >= 0) && (error_on_x_imis < 1))  << "Error on x is too high for IMIS at experience " << r;
            EXPECT_TRUE((error_reconstruction_imis >= 0) && (error_reconstruction_imis < 1))  << "Reconstruction error is too high for IMIS at experience " << r;
        }

    }

}

// int main(int argc, char **argv){
//    ::testing::InitGoogleTest(&argc, argv);
//    // testing::GTEST_FLAG(filter) = "*HapkeFunctionalTest*";
//    return RUN_ALL_TESTS();
// }
