//
// Created by reverse-proxy on 5‏/4‏/2020.
//

#include <gtest/gtest.h>
#include "../../src/functionalModel/FunctionalModel.h"
#include "../../src/dataGeneration/StatModel.h"
#include "../../src/dataGeneration/creators.h"
#include "../../src/learningModel/covariances/Icovariance.h"
#include "../../src/learningModel/initializers/MultInitializer.h"
#include "../../src/learningModel/initializers/FixedInitializer.h"
#include "../../src/learningModel/estimators/EmEstimator.h"
#include "../../src/learningModel/gllim/IGLLiMLearning.h"
#include "../../src/learningModel/gllim/GLLiMLearning.h"
#include "../../src/prediction/Predictor.h"
#include "../../src/prediction/PredictionResult.h"

#include "../../src/functionalModel/HapkeModel/HapkeVersions/Hapke02Model.h"
#include "../../src/functionalModel/HapkeModel/HapkeAdapters/SixParamsModel.h"
#include "../../src/functionalModel/HapkeModel/HapkeAdapters/ThreeParamsModel.h"
#include "../../src/functionalModel/HapkeModel/HapkeAdapters/FourParamsModel.h"
#include "../../src/importanceSampling/ImportanceSampler.h"
#include "../../src/importanceSampling/target/ISTargetDependent.h"
#include "../../src/importanceSampling/proposition/GaussianMixtureProposition.h"
#include "../../src/importanceSampling/proposition/GaussianRegularizedProposition.h"
#include <gtest/gtest.h>
#include <armadillo>
#include <iostream>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

using namespace learningModel;
using namespace Functional;
using namespace DataGeneration;
namespace pt = boost::property_tree;

class ExpoFunctional : public FunctionalModel{
public:
    int L, D;

    ExpoFunctional(int L, int D){
        this->L = L;
        this->D = D;
    }

    int get_D_dimension() override {
        return D;
    }

    int get_L_dimension() override {
        return L;
    }

    void to_physic(rowvec &x) override {
        x = x * 3 - 1;
    }

    void from_physic(double *x, int size) override{
        // nothing
    }

    void F(rowvec x, rowvec &y) override {
        to_physic(x);
        y = exp(x);
    }
};

class FourcheFunctional : public FunctionalModel{
public:
    int L, D;

    FourcheFunctional(int L, int D){
        this->L = L;
        this->D = D;
    }

    int get_D_dimension() override {
        return D;
    }

    int get_L_dimension() override {
        return L;
    }

    void to_physic(rowvec &x) override {
        // nothing
    }

    void from_physic(double *x, int size) override{
        // nothing
    }

    void F(rowvec x, rowvec &y) override {
        for(unsigned i=0; i<x.n_cols; i++){
            if(i == 0)
                y(i) = pow(x(i) - 0.5, 2);
            else
                y(i) = x(i);
        }
    }
};

template <typename T , typename U >
struct ExperienceConfig{
    unsigned N_train;
    unsigned N_test;
    vec var_obs;
    unsigned K;
    unsigned K_merged;
    double threshold;
    unsigned D;
    unsigned L;
    double norm;
    unsigned r_noise;
    bool is = false;
    std::shared_ptr<FunctionalModel> functionalModel;
    std::shared_ptr<StatModel> statModel;
    std::shared_ptr<GLLiMLearning<T,U>> gllim;
};

struct ExperienceResult{
    double F_mean = 0;
    double Me_mean = 0;
    double Ce_mean = 0;
    double Yme_mean = 0;
    double Yb_mean = 0;
    double Yce_mean = 0;
    double F_median = 0;
    double Me_median = 0;
    double Ce_median = 0;
    double Yme_median = 0;
    double Yb_median = 0;
    double Yce_median = 0;
    double V1 = 0;
    double V2 = 0;
};

template <typename T , typename U >
ExperienceResult executeExperience(ExperienceConfig<T,U> config){
    ExperienceResult result;

    auto start0 = std::chrono::high_resolution_clock::now();
    auto start1 = std::chrono::high_resolution_clock::now();
    auto start2 = std::chrono::high_resolution_clock::now();
    auto start3 = std::chrono::high_resolution_clock::now();
    auto start4 = std::chrono::high_resolution_clock::now();

    auto end0 = std::chrono::high_resolution_clock::now();
    auto end1 = std::chrono::high_resolution_clock::now();
    auto end2 = std::chrono::high_resolution_clock::now();
    auto end3 = std::chrono::high_resolution_clock::now();


    auto duration0 = std::chrono::duration_cast<std::chrono::milliseconds>(end0 - start0);
    auto duration1 = std::chrono::duration_cast<std::chrono::milliseconds>(end1 - start1);
    auto duration2 = std::chrono::duration_cast<std::chrono::milliseconds>(end2 - start2);
    auto duration3 = std::chrono::duration_cast<std::chrono::milliseconds>(end3 - start3);


    std::cout << "Generate test data :" << std::endl;
    std::tuple<mat, mat> data = config.statModel->gen_data(config.N_train);
    rowvec y_temp_1(config.D);

    mat x_test(config.N_test,config.L, fill::ones);
    x_test *= 0.5;
    mat y_test(config.N_test, config.D);

    for(unsigned n=0; n<config.N_test; n++){

        config.functionalModel->F(x_test.row(n), y_temp_1);
        y_test.row(n) = y_temp_1;
    }

    std::cout << "Init GLLiM :" << std::endl;
    start0 = std::chrono::high_resolution_clock::now();
    config.gllim->initialize(std::get<0>(data),std::get<1>(data));
    end0 = std::chrono::high_resolution_clock::now();
    duration0 = std::chrono::duration_cast<std::chrono::milliseconds>(end0 - start0);

    std::cout << "init : " << duration0.count() << std::endl;


    std::cout << "Train GLLiM :" << std::endl;
    start1 = std::chrono::high_resolution_clock::now();
    config.gllim->train(std::get<0>(data),std::get<1>(data));
    end1 = std::chrono::high_resolution_clock::now();
    duration1 = std::chrono::duration_cast<std::chrono::milliseconds>(end1 - start1);

    std::cout << "train : " << duration1.count() << std::endl;

    result.F_mean = 0;
    result.Me_mean = 0;
    result.Ce_mean = 0;
    result.Yme_mean = 0;
    result.Yce_mean = 0;
    result.Yb_mean = 0;
    result.F_median = 0;
    result.Me_median = 0;
    result.Ce_median = 0;
    result.Yme_median = 0;
    result.Yce_median = 0;
    result.Yb_median = 0;
    result.V1 = 0;
    result.V2 = 0;

    prediction::Predictor predictorByCenters(std::dynamic_pointer_cast<IGLLiMLearning>(config.gllim), config.K_merged, 10, config.threshold);

    vec cov_is(config.D, fill::ones);
    vec estimation(config.D,fill::zeros);

    duration0 = std::chrono::duration_cast<std::chrono::milliseconds>(start0 - start0);
    duration1 = std::chrono::duration_cast<std::chrono::milliseconds>(start1 - start1);

    std::cout << "Predict :" << std::endl;
    importanceSampling::ISTarget target;
    target.setTarget(config.statModel);
    importanceSampling::ImportanceSampler sampler(1000, std::make_shared<importanceSampling::ISTarget>(target));


    cube allPred(config.L, config.K_merged, config.N_test);
    for(unsigned n=0; n<config.N_test; n++){
        start2 = std::chrono::high_resolution_clock::now();
        prediction::PredictionResult predics = predictorByCenters.predict(y_test.row(n).t(), config.var_obs);
        end2 = std::chrono::high_resolution_clock::now();
        duration2 += std::chrono::duration_cast<std::chrono::milliseconds>(end2 - start2);

        //Mean
        arma::gmm_full gmm = config.gllim->logDensity(config.gllim->getParameters(), x_test.row(n).t());
        estimation.fill(0);
        for(unsigned k=0; k<gmm.hefts.n_cols; k++){
            estimation += gmm.hefts(k) * gmm.means.col(k);
        }
        unsigned cpt1 = 0;
        for(auto element : predics.meanPredResult.mean){
            if(element <= 1 && element >= 0)
                cpt1 ++;
        }
        if(cpt1 == config.L){
            if(config.is){
                start1 = std::chrono::high_resolution_clock::now();

                importanceSampling::GaussianMixtureProposition prop(predics.meanPredResult.gmm_weights,
                        predics.meanPredResult.gmm_means,
                        predics.meanPredResult.gmm_covs);
                importanceSampling::ISResult res = sampler.execute(
                        std::make_shared<importanceSampling::GaussianMixtureProposition>(prop),
                        y_test.row(n).t(),
                        cov_is / 1000);

                config.functionalModel->F(res.mean.t(), y_temp_1);
                end1 = std::chrono::high_resolution_clock::now();
                duration1 += std::chrono::duration_cast<std::chrono::milliseconds>(end1 - start1);

                result.F_mean += max(abs((estimation - y_test.row(n))));//config.norm);
                result.Me_mean += max(abs((res.mean - x_test.row(n).t())));
                result.Yme_mean += max(abs((y_temp_1 - y_test.row(n))));//config.norm));
            }else{
                config.functionalModel->F(predics.meanPredResult.mean.t(), y_temp_1);
                result.F_mean += max(abs((estimation - y_test.row(n))));
                result.Me_mean += max(abs((predics.meanPredResult.mean - x_test.row(n).t())));
                result.Yme_mean += max(abs((y_temp_1 - y_test.row(n))));
            }
            result.V1 += 1;
        }

        //Centers
        double Ce = datum::inf, Yb = datum::inf;
        double V2 = 0;
        for(unsigned k=0; k<config.K_merged; k++){
            //allPred.slice(n).col(k) = predics[k].first;
            unsigned cpt2 = 0;
            for(auto element : predics.centerPredResult.means.col(k)){
                if(element <= 1 && element >= 0)
                    cpt2 ++;
            }
            if(cpt2 == config.L){
                V2 += 1;
                if(config.is){
                    start3 = std::chrono::high_resolution_clock::now();
                    vec center = predics.centerPredResult.means.col(k);
                    importanceSampling::GaussianRegularizedProposition prop(center,
                                                                            predics.centerPredResult.covs.slice(k));
                    importanceSampling::ISResult res = sampler.execute(
                            std::make_shared<importanceSampling::GaussianRegularizedProposition>(prop),
                            y_test.row(n).t(),
                            cov_is / 1000);
                    config.functionalModel->F(res.mean.t(), y_temp_1);
                    end3 = std::chrono::high_resolution_clock::now();
                    duration3 += std::chrono::duration_cast<std::chrono::milliseconds>(end3 - start3);
                    result.Yce_mean += max(abs((y_temp_1 - y_test.row(n)))) / config.K_merged;
                    Ce = std::min(Ce, max(abs((res.mean- x_test.row(n).t()))));
                    Yb = std::min(Yb, max(abs((y_temp_1 - y_test.row(n)))));
                }else{

                    config.functionalModel->F(predics.centerPredResult.means.col(k).t(), y_temp_1);
                    result.Yce_mean += max(abs((y_temp_1 - y_test.row(n)))) / config.K_merged;
                    Ce = std::min(Ce, max(abs((predics.centerPredResult.means.col(k) - x_test.row(n).t()))));
                    Yb = std::min(Yb, max(abs((y_temp_1 - y_test.row(n)))));
                }
            }else{
                Ce = 0;
                Yb = 0;
            }
        }
        result.V2 += V2/config.K_merged;
        if(Ce != -datum::inf && Yb != -datum::inf){
            result.Ce_mean += Ce;
            result.Yb_mean += Yb;
        }
    }


    /*Mat<unsigned> regu = prediction::Predictor::regularize(allPred);
    cube allPred_reg(config.L, config.K_merged, config.N_test);
    for(unsigned i=0; i<config.N_test; i++){
        for(unsigned j=0 ; j<config.K_merged; j++){
            allPred_reg.slice(i).col(j) = allPred.slice(i).col(regu(i,j));
        }
    }*/

    /*allPred.print("all Pred");
    allPred_reg.print(" all Pred regu");*/

    auto end4 = std::chrono::high_resolution_clock::now();
    auto duration4 = std::chrono::duration_cast<std::chrono::milliseconds>(end4 - start4);

    std::cout << "pred mean : " << duration0.count() << std::endl;
    std::cout << "pred center : " << duration2.count() << std::endl;
    std::cout << "is mean : " << duration1.count() << std::endl;
    std::cout << "is center : " << duration3.count() << std::endl;
    std::cout << "all : " << duration4.count() << std::endl;

    result.F_mean /= config.N_test;
    result.Me_mean /= config.N_test;
    result.Ce_mean /= config.N_test;
    result.Yme_mean /= config.N_test;
    result.Yce_mean /= config.N_test;
    result.Yb_mean /= config.N_test;
    result.V1 /= config.N_test;
    result.V2 /= config.N_test;

    return result;
}


class LearningPerformanceTests :  public ::testing::Test{

protected:
    void SetUp()override {

        double geometries[47*3];
        unsigned i = 0;
        unsigned jump = 0;
        pt::ptree root;
        pt::read_json("../../../test_hapke.json", root);  // Load the json file in this ptree

        //Read geometries
        std::string variables[] = {"eme", "inc", "phi"};
        for(unsigned j=0; j<3; j++){
            i=0;
            jump = 0;
            for (pt::ptree::value_type& v : root.get_child(variables[j]))
            {
                if(jump>=3){
                    geometries[i*3+j] = stod(v.second.data());
                    i++;
                }
                jump++;
            }
        }

        myModel = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries, 47, 3,
                                                               std::shared_ptr<HapkeAdapter>(new SixParamsModel()),
                                                               30.0));


        std::shared_ptr<MultInitConfig> myConfig (
                new MultInitConfig(
                        123456789,
                        5,
                        3,
                        std::make_shared<GMMLearningConfig>(GMMLearningConfig(10,5,1e-08)),
                        std::make_shared<EMLearningConfig>(EMLearningConfig(10,5,1e-08))));
        MultInitializer<FullCovariance,DiagCovariance> initializer(myConfig);

        std::shared_ptr<FixedInitConfig> fixedConfig (
                new FixedInitConfig(
                        123456789,
                        std::make_shared<GMMLearningConfig>(GMMLearningConfig(0,5,1e-08)),
                        std::make_shared<EMLearningConfig>(EMLearningConfig(4,0,1e-08))
                ));

        std::shared_ptr<EMLearningConfig> myLearningconfig (new EMLearningConfig(30,2.0,1e-08));
        EmEstimator<FullCovariance, DiagCovariance> estimator(myLearningconfig);

        gllim = std::shared_ptr<GLLiMLearning<FullCovariance,DiagCovariance>>(
                new GLLiMLearning<FullCovariance,DiagCovariance>(
                        std::make_shared<MultInitializer<FullCovariance,DiagCovariance>>(initializer),
                        std::make_shared<EmEstimator<FullCovariance, DiagCovariance>>(estimator),100));

        gllimFF = std::shared_ptr<GLLiMLearning<FullCovariance,FullCovariance>>(
                new GLLiMLearning<FullCovariance,FullCovariance>(
                        std::shared_ptr<MultInitializer<FullCovariance,FullCovariance>>(
                                new MultInitializer<FullCovariance,FullCovariance>(myConfig)
                                ),
                        std::make_shared<GmmEstimator>(
                                std::make_shared<GMMLearningConfig>(GMMLearningConfig(0,10,1e-10))
                                        ),
                        100));

        gllimDD = std::shared_ptr<GLLiMLearning<DiagCovariance,DiagCovariance>>(
                new GLLiMLearning<DiagCovariance,DiagCovariance>(
                        std::shared_ptr<MultInitializer<DiagCovariance,DiagCovariance>>(
                                new MultInitializer<DiagCovariance,DiagCovariance>(myConfig)
                        ),
                        std::shared_ptr<EmEstimator<DiagCovariance, DiagCovariance>>(
                                new EmEstimator<DiagCovariance, DiagCovariance>(myLearningconfig)
                        ),
                        10));

        gllimII = std::shared_ptr<GLLiMLearning<IsoCovariance,IsoCovariance>>(
                new GLLiMLearning<IsoCovariance,IsoCovariance>(
                        std::shared_ptr<MultInitializer<IsoCovariance,IsoCovariance>>(
                                new MultInitializer<IsoCovariance,IsoCovariance>(myConfig)
                        ),
                        std::shared_ptr<EmEstimator<IsoCovariance, IsoCovariance>>(
                                new EmEstimator<IsoCovariance, IsoCovariance>(myLearningconfig)
                        ),
                        100));

        gllimFD = std::shared_ptr<GLLiMLearning<FullCovariance,DiagCovariance>>(
                new GLLiMLearning<FullCovariance,DiagCovariance>(
                        std::shared_ptr<MultInitializer<FullCovariance,DiagCovariance>>(
                                new MultInitializer<FullCovariance,DiagCovariance>(myConfig)
                        ),
                        std::shared_ptr<EmEstimator<FullCovariance,DiagCovariance>>(
                                new EmEstimator<FullCovariance, DiagCovariance>(myLearningconfig)
                        ),
                        100));

        gllimFI = std::shared_ptr<GLLiMLearning<FullCovariance,IsoCovariance>>(
                new GLLiMLearning<FullCovariance,IsoCovariance>(
                        std::shared_ptr<MultInitializer<FullCovariance,IsoCovariance>>(
                                new MultInitializer<FullCovariance,IsoCovariance>(myConfig)
                        ),
                        std::shared_ptr<EmEstimator<FullCovariance,IsoCovariance>>(
                                new EmEstimator<FullCovariance, IsoCovariance>(myLearningconfig)
                        ),
                        100));

        gllimDI = std::shared_ptr<GLLiMLearning<DiagCovariance,IsoCovariance>>(
                new GLLiMLearning<DiagCovariance,IsoCovariance>(
                        std::shared_ptr<MultInitializer<DiagCovariance,IsoCovariance>>(
                                new MultInitializer<DiagCovariance,IsoCovariance>(myConfig)
                        ),
                        std::shared_ptr<EmEstimator<DiagCovariance,IsoCovariance>>(
                                new EmEstimator<DiagCovariance, IsoCovariance>(myLearningconfig)
                        ),
                        100));

        gllimDF = std::shared_ptr<GLLiMLearning<DiagCovariance,FullCovariance>>(
                new GLLiMLearning<DiagCovariance,FullCovariance>(
                        std::shared_ptr<MultInitializer<DiagCovariance,FullCovariance>>(
                                new MultInitializer<DiagCovariance,FullCovariance>(myConfig)
                        ),
                        std::shared_ptr<EmEstimator<DiagCovariance,FullCovariance>>(
                                new EmEstimator<DiagCovariance, FullCovariance>(myLearningconfig)
                        ),
                        100));

        gllimIF = std::shared_ptr<GLLiMLearning<IsoCovariance,FullCovariance>>(
                new GLLiMLearning<IsoCovariance,FullCovariance>(
                        std::shared_ptr<MultInitializer<IsoCovariance,FullCovariance>>(
                                new MultInitializer<IsoCovariance,FullCovariance>(myConfig)
                        ),
                        std::shared_ptr<EmEstimator<IsoCovariance, FullCovariance>>(
                                new EmEstimator<IsoCovariance, FullCovariance>(myLearningconfig)
                        ),
                        100));

        gllimID = std::shared_ptr<GLLiMLearning<IsoCovariance,DiagCovariance>>(
                new GLLiMLearning<IsoCovariance,DiagCovariance>(
                        std::shared_ptr<MultInitializer<IsoCovariance,DiagCovariance>>(
                                new MultInitializer<IsoCovariance,DiagCovariance>(myConfig)
                        ),
                        std::shared_ptr<EmEstimator<IsoCovariance, DiagCovariance>>(
                                new EmEstimator<IsoCovariance, DiagCovariance>(myLearningconfig)
                        ),
                        100));
    }

    std::shared_ptr<FunctionalModel> myModel;
    std::shared_ptr<StatModel> statModel;
    std::shared_ptr<GLLiMLearning<FullCovariance,DiagCovariance>> gllim;

    std::shared_ptr<GLLiMLearning<FullCovariance,FullCovariance>> gllimFF;
    std::shared_ptr<GLLiMLearning<FullCovariance,DiagCovariance>> gllimFD;
    std::shared_ptr<GLLiMLearning<FullCovariance,IsoCovariance>> gllimFI;
    std::shared_ptr<GLLiMLearning<DiagCovariance,DiagCovariance>> gllimDD;
    std::shared_ptr<GLLiMLearning<DiagCovariance,FullCovariance>> gllimDF;
    std::shared_ptr<GLLiMLearning<DiagCovariance,IsoCovariance>> gllimDI;
    std::shared_ptr<GLLiMLearning<IsoCovariance,IsoCovariance>> gllimII;
    std::shared_ptr<GLLiMLearning<IsoCovariance,FullCovariance>> gllimIF;
    std::shared_ptr<GLLiMLearning<IsoCovariance,DiagCovariance>> gllimID;

};

TEST_F(LearningPerformanceTests, FourcheTest){
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.norm = 0.25;
    config.K = 100;
    config.K_merged = 2;
    config.r_noise = 1000000;
    config.N_train = 1000;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.N_test = 10;
    config.is = true;
    for(unsigned i=1; i<2; i+=2){
        config.L = i;
        config.D = i;
        config.var_obs = vec(config.D, fill::zeros);
        config.functionalModel = std::shared_ptr<FunctionalModel>(new FourcheFunctional(config.L, config.D));
        config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());

        ExperienceResult result = executeExperience(config);
        std::cout << "Experience : D , L : "<< i << std::endl;
        std::cout << result.F_mean * 100 << std::endl;
        std::cout << result.Me_mean * 100 << std::endl;
        std::cout << result.Ce_mean * 100 << std::endl;
        std::cout << result.Yme_mean * 100 << std::endl;
        std::cout << result.Yce_mean * 100 << std::endl;
        std::cout << result.Yb_mean * 100 << std::endl;
        std::cout << result.V1 * 100 << std::endl;
        std::cout << result.V2 * 100 << std::endl;
    }
}

TEST_F(LearningPerformanceTests, Experience2){
    // Cov types
    ExperienceConfig<FullCovariance, FullCovariance> configFF;
    configFF.norm = 0.25;
    configFF.K = 100;
    configFF.K_merged = 2;
    configFF.r_noise = 1000000;
    configFF.N_train = 50000;
    configFF.N_test = 50000;
    configFF.threshold = 0.01;
    configFF.L = 1;
    configFF.D = 1;
    configFF.var_obs = vec(configFF.D, fill::zeros);
    configFF.functionalModel = std::shared_ptr<FunctionalModel>(new FourcheFunctional(configFF.L, configFF.D));
    configFF.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configFF.functionalModel, configFF.r_noise , 1234).create());
    configFF.gllim = gllimFF;
    ExperienceResult result = executeExperience(configFF);
    std::cout << " Gllim<Full, Full> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<DiagCovariance, DiagCovariance> configDD;
    configDD.norm = 0.25;
    configDD.K = 100;
    configDD.K_merged = 2;
    configDD.r_noise = 1000000;
    configDD.N_train = 50000;
    configDD.N_test = 50000;
    configDD.threshold = 0.01;
    configDD.L = 1;
    configDD.D = 1;
    configDD.var_obs = vec(configDD.D, fill::zeros);
    configDD.functionalModel = std::shared_ptr<FunctionalModel>(new FourcheFunctional(configDD.L, configDD.D));
    configDD.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configDD.functionalModel, configDD.r_noise , 1234).create());
    configDD.gllim = gllimDD;
    result = executeExperience(configDD);
    std::cout << " Gllim<Diag, Diag> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<IsoCovariance, IsoCovariance> configII;
    configII.norm = 0.25;
    configII.K = 100;
    configII.K_merged = 2;
    configII.r_noise = 1000000;
    configII.N_train = 50000;
    configII.N_test = 50000;
    configII.threshold = 0.01;
    configII.L = 1;
    configII.D = 1;
    configII.var_obs = vec(configII.D, fill::zeros);
    configII.functionalModel = std::shared_ptr<FunctionalModel>(new FourcheFunctional(configII.L, configII.D));
    configII.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configII.functionalModel, configII.r_noise , 1234).create());
    configII.gllim = gllimII;
    result = executeExperience(configII);
    std::cout << " Gllim<Iso, Iso> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<IsoCovariance, DiagCovariance> configID;
    configID.norm = 0.25;
    configID.K = 100;
    configID.K_merged = 2;
    configID.r_noise = 1000000;
    configID.N_train = 50000;
    configID.N_test = 50000;
    configID.threshold = 0.01;
    configID.L = 1;
    configID.D = 1;
    configID.var_obs = vec(configID.D, fill::zeros);
    configID.functionalModel = std::shared_ptr<FunctionalModel>(new FourcheFunctional(configID.L, configID.D));
    configID.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configID.functionalModel, configID.r_noise , 1234).create());
    configID.gllim = gllimID;
    result = executeExperience(configID);
    std::cout << " Gllim<Iso, IDiag> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<IsoCovariance, FullCovariance> configIF;
    configIF.norm = 0.25;
    configIF.K = 100;
    configIF.K_merged = 2;
    configIF.r_noise = 1000000;
    configIF.N_train = 50000;
    configIF.N_test = 50000;
    configIF.threshold = 0.01;
    configIF.L = 1;
    configIF.D = 1;
    configIF.var_obs = vec(configIF.D, fill::zeros);
    configIF.functionalModel = std::shared_ptr<FunctionalModel>(new FourcheFunctional(configIF.L, configIF.D));
    configIF.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configIF.functionalModel, configIF.r_noise , 1234).create());
    configIF.gllim = gllimIF;
    result = executeExperience(configIF);
    std::cout << " Gllim<Iso, Full> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<FullCovariance, IsoCovariance> configFI;
    configFI.norm = 0.25;
    configFI.K = 100;
    configFI.K_merged = 2;
    configFI.r_noise = 1000000;
    configFI.N_train = 50000;
    configFI.N_test = 50000;
    configFI.threshold = 0.01;
    configFI.L = 1;
    configFI.D = 1;
    configFI.var_obs = vec(configFI.D, fill::zeros);
    configFI.functionalModel = std::shared_ptr<FunctionalModel>(new FourcheFunctional(configFI.L, configFI.D));
    configFI.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configFI.functionalModel, configFI.r_noise , 1234).create());
    configFI.gllim = gllimFI;
    result = executeExperience(configFI);
    std::cout << " Gllim<Full, Iso> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<DiagCovariance, IsoCovariance> configDI;
    configDI.norm = 0.25;
    configDI.K = 100;
    configDI.K_merged = 2;
    configDI.r_noise = 1000000;
    configDI.N_train = 50000;
    configDI.N_test = 50000;
    configDI.threshold = 0.01;
    configDI.L = 1;
    configDI.D = 1;
    configDI.var_obs = vec(configDI.D, fill::zeros);
    configDI.functionalModel = std::shared_ptr<FunctionalModel>(new FourcheFunctional(configDI.L, configDI.D));
    configDI.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configDI.functionalModel, configDI.r_noise , 1234).create());
    configDI.gllim = gllimDI;
    result = executeExperience(configDI);
    std::cout << " Gllim<Diag, Iso> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<FullCovariance, DiagCovariance> configFD;
    configFD.norm = 0.25;
    configFD.K = 100;
    configFD.K_merged = 2;
    configFD.r_noise = 1000000;
    configFD.N_train = 50000;
    configFD.N_test = 50000;
    configFD.threshold = 0.01;
    configFD.L = 1;
    configFD.D = 1;
    configFD.var_obs = vec(configFD.D, fill::zeros);
    configFD.functionalModel = std::shared_ptr<FunctionalModel>(new FourcheFunctional(configFD.L, configFD.D));
    configFD.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configFD.functionalModel, configFD.r_noise , 1234).create());
    configFD.gllim = gllimFD;
    result = executeExperience(configFD);
    std::cout << " Gllim<Full, Diag> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<DiagCovariance, FullCovariance> configDF;
    configDF.norm = 0.25;
    configDF.K = 100;
    configDF.K_merged = 2;
    configDF.r_noise = 1000000;
    configDF.N_train = 50000;
    configDF.N_test = 50000;
    configDF.threshold = 0.01;
    configDF.L = 1;
    configDF.D = 1;
    configDF.var_obs = vec(configDF.D, fill::zeros);
    configDF.functionalModel = std::shared_ptr<FunctionalModel>(new FourcheFunctional(configDF.L, configDF.D));
    configDF.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configDF.functionalModel, configDF.r_noise, 1234).create());
    configDF.gllim = gllimDF;
    result = executeExperience(configDF);
    std::cout << " Gllim<Diag, Full> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;
}

TEST_F(LearningPerformanceTests, Experience4) {
    // K, N
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.norm = exp(2) - exp(-1);
    config.K = 100;
    config.K_merged = 1;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.N_test = 50000;
    config.K = 100;
    config.N_train = 50000;
    config.L = 3;
    config.D = 3;
    config.var_obs = vec(config.D, fill::zeros);
    config.functionalModel = std::shared_ptr<FunctionalModel>(new ExpoFunctional(config.L, config.D));

    for(unsigned i=10; i<=100; i+=10){
        config.r_noise = i;
        config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());
        ExperienceResult result = executeExperience(config);
        std::cout << "r_noise : "<< i << std::endl;
        std::cout << result.F_mean * 100 << std::endl;
        std::cout << result.Me_mean * 100 << std::endl;
        std::cout << result.Ce_mean * 100 << std::endl;
        std::cout << result.Yme_mean * 100 << std::endl;
        std::cout << result.Yce_mean * 100 << std::endl;
        std::cout << result.Yb_mean * 100 << std::endl;
        std::cout << result.V1 * 100 << std::endl;
        std::cout << result.V2 * 100 << std::endl;
    }
}

TEST_F(LearningPerformanceTests, Experience3) {
    // K, N
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.norm = exp(2) - exp(-1);
    config.K = 100;
    config.K_merged = 1;
    config.r_noise = 1000000;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.N_test = 10000;
    config.L = 3;
    config.D = 3;
    config.var_obs = vec(config.D, fill::zeros);
    config.functionalModel = std::shared_ptr<FunctionalModel>(new ExpoFunctional(config.L, config.D));
    config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,1000000000 , 12345).create());

    for(unsigned i=5; i<=100; i+=5){

        config.K = i;
        config.N_train = 10*i;
        ExperienceResult result = executeExperience(config);
        std::cout << "K : "<< i << std::endl;
        std::cout << result.F_mean * 100 << std::endl;
        std::cout << result.Me_mean * 100 << std::endl;
        std::cout << result.Ce_mean * 100 << std::endl;
        std::cout << result.Yme_mean * 100 << std::endl;
        std::cout << result.Yce_mean * 100 << std::endl;
        std::cout << result.Yb_mean * 100 << std::endl;
        std::cout << result.V1 * 100 << std::endl;
        std::cout << result.V2 * 100 << std::endl;
    }
}

TEST_F(LearningPerformanceTests, Experience5) {
    // K, N
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.norm = exp(2) - exp(-1);
    config.K = 100;
    config.K_merged = 1;
    config.threshold = 0.01;
    config.N_test = 50000;
    config.K = 100;
    config.N_train = 50000;
    config.L = 3;
    config.D = 3;
    config.var_obs = vec(config.D, fill::zeros);
    config.r_noise = 100000;
    config.functionalModel = std::shared_ptr<FunctionalModel>(new ExpoFunctional(config.L, config.D));
    config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());

    std::shared_ptr<MultInitConfig> myConfig (
            new MultInitConfig(
                    123456789,
                    3,
                    3,
                    std::make_shared<GMMLearningConfig>(GMMLearningConfig(0,10,1e-08)),
                    std::make_shared<EMLearningConfig>(EMLearningConfig(10,5,1e-08))));

    for(unsigned i=1; i<=6; i++){
        std::shared_ptr<EMLearningConfig> myLearningconfig (new EMLearningConfig(10,2.0,1e-08 * pow(10, i)));

        config.gllim = std::shared_ptr<GLLiMLearning<DiagCovariance,DiagCovariance>>(
                new GLLiMLearning<DiagCovariance,DiagCovariance>(
                        std::shared_ptr<MultInitializer<DiagCovariance,DiagCovariance>>(
                                new MultInitializer<DiagCovariance,DiagCovariance>(myConfig)
                        ),
                        std::shared_ptr<EmEstimator<DiagCovariance, DiagCovariance>>(
                                new EmEstimator<DiagCovariance, DiagCovariance>(myLearningconfig)
                        ),
                        100));

        ExperienceResult result = executeExperience(config);
        std::cout << "iteration : "<< i << std::endl;
        std::cout << result.F_mean * 100 << std::endl;
        std::cout << result.Me_mean * 100 << std::endl;
        std::cout << result.Ce_mean * 100 << std::endl;
        std::cout << result.Yme_mean * 100 << std::endl;
        std::cout << result.Yce_mean * 100 << std::endl;
        std::cout << result.Yb_mean * 100 << std::endl;
        std::cout << result.V1 * 100 << std::endl;
        std::cout << result.V2 * 100 << std::endl;
    }
}

TEST_F(LearningPerformanceTests, Experience7) {
    // K, N
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.norm = exp(2) - exp(-1);
    config.K = 100;
    config.K_merged = 1;
    config.threshold = 0.01;
    config.N_test = 50000;
    config.K = 100;
    config.N_train = 50000;
    config.L = 3;
    config.D = 3;
    config.var_obs = vec(config.D, fill::zeros);
    config.r_noise = 100000;
    config.functionalModel = std::shared_ptr<FunctionalModel>(new ExpoFunctional(config.L, config.D));
    config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());

    for(unsigned i=1; i<=10; i+=2){
        std::shared_ptr<MultInitConfig> myConfig (
                new MultInitConfig(
                        123456789,
                        5,
                        i,
                        std::make_shared<GMMLearningConfig>(GMMLearningConfig(0,10,1e-08)),
                        std::make_shared<EMLearningConfig>(EMLearningConfig(10,5,1e-08))));

        std::shared_ptr<EMLearningConfig> myLearningconfig (new EMLearningConfig(10,2.0,1e-08));

        config.gllim = std::shared_ptr<GLLiMLearning<DiagCovariance,DiagCovariance>>(
                new GLLiMLearning<DiagCovariance,DiagCovariance>(
                        std::shared_ptr<MultInitializer<DiagCovariance,DiagCovariance>>(
                                new MultInitializer<DiagCovariance,DiagCovariance>(myConfig)
                        ),
                        std::shared_ptr<EmEstimator<DiagCovariance, DiagCovariance>>(
                                new EmEstimator<DiagCovariance, DiagCovariance>(myLearningconfig)
                        ),
                        100));

        ExperienceResult result = executeExperience(config);
        std::cout << "iteration : "<< i << std::endl;
        std::cout << result.F_mean * 100 << std::endl;
        std::cout << result.Me_mean * 100 << std::endl;
        std::cout << result.Ce_mean * 100 << std::endl;
        std::cout << result.Yme_mean * 100 << std::endl;
        std::cout << result.Yce_mean * 100 << std::endl;
        std::cout << result.Yb_mean * 100 << std::endl;
        std::cout << result.V1 * 100 << std::endl;
        std::cout << result.V2 * 100 << std::endl;
    }
}

TEST_F(LearningPerformanceTests, Experience6){
    // D, L
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.norm = exp(2) - exp(-1);
    config.K = 100;
    config.K_merged = 1;
    config.r_noise = 1000000;
    config.N_train = 500;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.N_test = 50;
    config.is = false;

    for(unsigned i=1; i<10; i+=2){
        config.L = i;
        config.D = i;
        config.var_obs = vec(config.D, fill::zeros);
        config.functionalModel = std::shared_ptr<FunctionalModel>(new ExpoFunctional(config.L, config.D));
        config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,1000000000 , 12345).create());
        auto start = std::chrono::high_resolution_clock::now();
        ExperienceResult result = executeExperience(config);
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        std::cout << "totale time experience : " << duration.count() << std::endl;
        std::cout << "Experience without is : D , L : "<< i << std::endl;
        std::cout << result.F_mean * 100 << std::endl;
        std::cout << result.Me_mean * 100 << std::endl;
        std::cout << result.Ce_mean * 100 << std::endl;
        std::cout << result.Yme_mean * 100 << std::endl;
        std::cout << result.Yce_mean * 100 << std::endl;
        std::cout << result.Yb_mean * 100 << std::endl;
        std::cout << result.V1 * 100 << std::endl;
        std::cout << result.V2 * 100 << std::endl;
    }

    /*config.is = true;
    for(unsigned i=1; i<10; i+=2){
        config.N_test = 100 *i;
        config.L = 5;
        config.D = 5;
        config.var_obs = vec(config.D, fill::zeros);
        config.functionalModel = std::shared_ptr<FunctionalModel>(new ExpoFunctional(config.L, config.D));
        config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,1000000000 , 12345).create());
        auto start = std::chrono::high_resolution_clock::now();
        ExperienceResult result = executeExperience(config);
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        std::cout << "totale time experience : " << duration.count() << std::endl;
        std::cout << "Experience with is : D , L : "<< 5 << std::endl;
        std::cout << result.F_mean * 100 << std::endl;
        std::cout << result.Me_mean * 100 << std::endl;
        std::cout << result.Ce_mean * 100 << std::endl;
        std::cout << result.Yme_mean * 100 << std::endl;
        std::cout << result.Yce_mean * 100 << std::endl;
        std::cout << result.Yb_mean * 100 << std::endl;
        std::cout << result.V1 * 100 << std::endl;
        std::cout << result.V2 * 100 << std::endl;
    }*/
}

TEST_F(LearningPerformanceTests, HapkeFunctionalTest1){
    double geometries[47*3];
    unsigned i = 0;
    unsigned jump = 0;
    pt::ptree root;
    pt::read_json("../../../test_hapke.json", root);  // Load the json file in this ptree

    //Read geometries
    std::string variables[] = {"eme", "inc", "phi"};
    for(unsigned j=0; j<3; j++){
        i=0;
        jump = 0;
        for (pt::ptree::value_type& v : root.get_child(variables[j]))
        {
            if(jump>=3){
                geometries[i*3+j] = stod(v.second.data());
                i++;
            }
            jump++;
        }
    }

    myModel = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries, 47, 3,
                                                             std::shared_ptr<HapkeAdapter>(new SixParamsModel()),
                                                             30.0));
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.is = true;
    /*config.norm = 1;
    config.K = 100;
    config.K_merged = 2;
    config.r_noise = 10000;
    config.N_train = 5000;
    config.N_test = 5000;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    config.L = myModel->get_L_dimension();
    config.D = myModel->get_D_dimension();
    config.var_obs = vec(config.D, fill::zeros);
    config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());
    ExperienceResult result = executeExperience(config);
    std::cout << " six params :" << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;*/



    myModel = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries, 47, 3,
                                                             std::shared_ptr<HapkeAdapter>(new FourParamsModel(0.5,0.5)),
                                                             30.0));
    config.norm = 1;
    config.K = 10;
    config.K_merged = 2;
    config.r_noise = 10000;
    config.N_train = 500;
    config.N_test = 1;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.L = myModel->get_L_dimension();
    config.D = myModel->get_D_dimension();
    config.var_obs = vec(config.D, fill::zeros);
    //config.var_obs /= 1000;
    config.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());
    ExperienceResult result = executeExperience(config);
    std::cout << " four params :" << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;


    /*myModel = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries, 47, 3,
                                                             std::shared_ptr<HapkeAdapter>(new ThreeParamsModel(0.5, 0.5)),
                                                             30.0));
    config.norm = 1;
    config.K = 100;
    config.K_merged = 2;
    config.r_noise = 10000;
    config.N_train = 5000;
    config.N_test = 5000;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.L = myModel->get_L_dimension();
    config.D = myModel->get_D_dimension();
    config.var_obs = vec(config.D, fill::zeros);
    config.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());
    result = executeExperience(config);
    std::cout << " three params :" << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;*/

}

TEST_F(LearningPerformanceTests, HapkeFunctionalTest2){
    double geometries[47*3];
    unsigned i = 0;
    unsigned jump = 0;

    pt::ptree root;
    pt::read_json("../../../test_hapke.json", root);  // Load the json file in this ptree

    //Read geometries
    std::string variables[] = {"eme", "inc", "phi"};
    for(unsigned j=0; j<3; j++){
        i=0;
        jump = 0;
        for (pt::ptree::value_type& v : root.get_child(variables[j]))
        {
            if(jump>=3){
                geometries[i*3+j] = stod(v.second.data());
                i++;
            }
            jump++;
        }
    }

    myModel = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries, 47, 3,
                                                             std::shared_ptr<HapkeAdapter>(new FourParamsModel(0.5,0.5)),
                                                             30.0));

    ExperienceConfig<FullCovariance, FullCovariance> configFF;
    configFF.norm = 1;
    configFF.K = 100;
    configFF.K_merged = 2;
    configFF.r_noise = 1000000;
    configFF.N_train = 5000;
    configFF.N_test = 50;
    configFF.threshold = 0.01;
    configFF.L = myModel->get_L_dimension();
    configFF.D = myModel->get_D_dimension();
    configFF.var_obs = vec(configFF.D, fill::zeros);
    configFF.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    configFF.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configFF.functionalModel, configFF.r_noise , 1234).create());
    configFF.gllim = gllimFF;
    ExperienceResult result = executeExperience(configFF);
    std::cout << " Gllim<Full, Full> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<DiagCovariance, DiagCovariance> configDD;
    configDD.norm = 1;
    configDD.K = 100;
    configDD.K_merged = 2;
    configDD.r_noise = 1000000;
    configDD.N_train = 5000;
    configDD.N_test = 50;
    configDD.threshold = 0.01;
    configDD.L = 4;
    configDD.D = 47;
    configDD.var_obs = vec(configDD.D, fill::zeros);
    configDD.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    configDD.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configDD.functionalModel, configDD.r_noise , 1234).create());
    configDD.gllim = gllimDD;
    result = executeExperience(configDD);
    std::cout << " Gllim<Diag, Diag> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<IsoCovariance, IsoCovariance> configII;
    configII.norm = 1;
    configII.K = 100;
    configII.K_merged = 2;
    configII.r_noise = 1000000;
    configII.N_train = 5000;
    configII.N_test = 50;
    configII.threshold = 0.01;
    configII.L = 4;
    configII.D = 47;
    configII.var_obs = vec(configII.D, fill::zeros);
    configII.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    configII.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configII.functionalModel, configII.r_noise , 1234).create());
    configII.gllim = gllimII;
    result = executeExperience(configII);
    std::cout << " Gllim<Iso, Iso> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<IsoCovariance, DiagCovariance> configID;
    configID.norm = 0.25;
    configID.K = 100;
    configID.K_merged = 2;
    configID.r_noise = 1000000;
    configID.N_train = 5000;
    configID.N_test = 50;
    configID.threshold = 0.01;
    configID.L = 4;
    configID.D = 47;
    configID.var_obs = vec(configID.D, fill::zeros);
    configID.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    configID.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configID.functionalModel, configID.r_noise , 1234).create());
    configID.gllim = gllimID;
    result = executeExperience(configID);
    std::cout << " Gllim<Iso, IDiag> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<IsoCovariance, FullCovariance> configIF;
    configIF.norm = 1;
    configIF.K = 100;
    configIF.K_merged = 2;
    configIF.r_noise = 1000000;
    configIF.N_train = 5000;
    configIF.N_test = 50;
    configIF.threshold = 0.01;
    configIF.L = 4;
    configIF.D = 47;
    configIF.var_obs = vec(configIF.D, fill::zeros);
    configIF.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    configIF.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configIF.functionalModel, configIF.r_noise , 1234).create());
    configIF.gllim = gllimIF;
    result = executeExperience(configIF);
    std::cout << " Gllim<Iso, Full> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<FullCovariance, IsoCovariance> configFI;
    configFI.norm = 1;
    configFI.K = 100;
    configFI.K_merged = 2;
    configFI.r_noise = 1000000;
    configFI.N_train = 5000;
    configFI.N_test = 50;
    configFI.threshold = 0.01;
    configFI.L = 4;
    configFI.D = 47;
    configFI.var_obs = vec(configFI.D, fill::zeros);
    configFI.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    configFI.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configFI.functionalModel, configFI.r_noise , 1234).create());
    configFI.gllim = gllimFI;
    result = executeExperience(configFI);
    std::cout << " Gllim<Full, Iso> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<DiagCovariance, IsoCovariance> configDI;
    configDI.norm = 1;
    configDI.K = 100;
    configDI.K_merged = 2;
    configDI.r_noise = 1000000;
    configDI.N_train = 5000;
    configDI.N_test = 50;
    configDI.threshold = 0.01;
    configDI.L = 4;
    configDI.D = 47;
    configDI.var_obs = vec(configDI.D, fill::zeros);
    configDI.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    configDI.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configDI.functionalModel, configDI.r_noise , 1234).create());
    configDI.gllim = gllimDI;
    result = executeExperience(configDI);
    std::cout << " Gllim<Diag, Iso> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<FullCovariance, DiagCovariance> configFD;
    configFD.norm = 1;
    configFD.K = 100;
    configFD.K_merged = 2;
    configFD.r_noise = 1000000;
    configFD.N_train = 5000;
    configFD.N_test = 50;
    configFD.threshold = 0.01;
    configFD.L = 4;
    configFD.D = 47;
    configFD.var_obs = vec(configFD.D, fill::zeros);
    configFD.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    configFD.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configFD.functionalModel, configFD.r_noise , 1234).create());
    configFD.gllim = gllimFD;
    result = executeExperience(configFD);
    std::cout << " Gllim<Full, Diag> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    ExperienceConfig<DiagCovariance, FullCovariance> configDF;
    configDF.norm = 1;
    configDF.K = 100;
    configDF.K_merged = 2;
    configDF.r_noise = 1000000;
    configDF.N_train = 5000;
    configDF.N_test = 50;
    configDF.threshold = 0.01;
    configDF.L = 4;
    configDF.D = 47;
    configDF.var_obs = vec(configDF.D, fill::zeros);
    configDF.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    configDF.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", configDF.functionalModel, configDF.r_noise, 1234).create());
    configDF.gllim = gllimDF;
    result = executeExperience(configDF);
    std::cout << " Gllim<Diag, Full> : " << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;



}

TEST_F(LearningPerformanceTests, HapkeFunctionalTest3){

    double geometries[47*3];
    unsigned i = 0;
    unsigned jump = 0;
    pt::ptree root;
    pt::read_json("../../../test_hapke.json", root);  // Load the json file in this ptree

    //Read geometries
    std::string variables[] = {"eme", "inc", "phi"};
    for(unsigned j=0; j<3; j++){
        i=0;
        jump = 0;
        for (pt::ptree::value_type& v : root.get_child(variables[j]))
        {
            if(jump>=3){
                geometries[i*3+j] = stod(v.second.data());
                i++;
            }
            jump++;
        }
    }

    myModel = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries, 47, 3,
                                                             std::shared_ptr<HapkeAdapter>(new FourParamsModel(0.5,0.5)),
                                                             30.0));
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.norm = 1;
    config.K = 100;
    config.K_merged = 3;
    config.r_noise = 10000;
    config.N_test = 5000;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.L = myModel->get_L_dimension();
    config.D = myModel->get_D_dimension();
    config.var_obs = vec(config.D, fill::zeros);
    config.functionalModel = std::shared_ptr<FunctionalModel>(myModel);
    config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());
    for(unsigned i=1 ; i<=5 ; i++){
        config.N_train = 10000* i;
        ExperienceResult result = executeExperience(config);
        std::cout << " N train :" << 10000* i << std::endl;
        std::cout << result.F_mean * 100 << std::endl;
        std::cout << result.Me_mean * 100 << std::endl;
        std::cout << result.Ce_mean * 100 << std::endl;
        std::cout << result.Yme_mean * 100 << std::endl;
        std::cout << result.Yce_mean * 100 << std::endl;
        std::cout << result.Yb_mean * 100 << std::endl;
        std::cout << result.V1 * 100 << std::endl;
        std::cout << result.V2 * 100 << std::endl;
    }
}

TEST_F(LearningPerformanceTests, HapkeFunctionalTest4){

    double geometries[47*3];
    unsigned i = 0;
    unsigned jump = 0;
    pt::ptree root;
    pt::read_json("../../../test_hapke.json", root);  // Load the json file in this ptree

    //Read geometries
    std::string variables[] = {"eme", "inc", "phi"};
    for(unsigned j=0; j<3; j++){
        i=0;
        jump = 0;
        for (pt::ptree::value_type& v : root.get_child(variables[j]))
        {
            if(jump>=3){
                geometries[i*3+j] = stod(v.second.data());
                i++;
            }
            jump++;
        }
    }

    myModel = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries, 47, 3,
                                                             std::shared_ptr<HapkeAdapter>(new FourParamsModel(0.5,0.5)),
                                                             30.0));
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.norm = 1;
    config.K = 100;
    config.K_merged = 3;
    config.N_train = 1000;
    config.N_test = 50;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.L = myModel->get_L_dimension();
    config.D = myModel->get_D_dimension();
    config.var_obs = vec(config.D, fill::ones);
    config.var_obs /= 100;
    config.functionalModel = std::shared_ptr<FunctionalModel>(myModel);

    for(unsigned i=1 ; i<=7 ; i++){
        config.r_noise = 10 * i;
        config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());
        ExperienceResult result = executeExperience(config);
        std::cout << " noise : "<< config.r_noise  << std::endl;
        std::cout << result.F_mean * 100 << std::endl;
        std::cout << result.Me_mean * 100 << std::endl;
        std::cout << result.Ce_mean * 100 << std::endl;
        std::cout << result.Yme_mean * 100 << std::endl;
        std::cout << result.Yce_mean * 100 << std::endl;
        std::cout << result.Yb_mean * 100 << std::endl;
        std::cout << result.V1 * 100 << std::endl;
        std::cout << result.V2 * 100 << std::endl;
    }
}

TEST_F(LearningPerformanceTests, HapkeFunctionalTest5){

    double geometries[47*3];
    unsigned i = 0;
    unsigned jump = 0;
    pt::ptree root;
    pt::read_json("../../../test_hapke.json", root);  // Load the json file in this ptree

    //Read geometries
    std::string variables[] = {"eme", "inc", "phi"};
    for(unsigned j=0; j<3; j++){
        i=0;
        jump = 0;
        for (pt::ptree::value_type& v : root.get_child(variables[j]))
        {
            if(jump>=3){
                geometries[i*3+j] = stod(v.second.data());
                i++;
            }
            jump++;
        }
    }

    myModel = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries, 47, 3,
                                                             std::shared_ptr<HapkeAdapter>(new FourParamsModel(0.5,0.5)),
                                                             30.0));
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.norm = 1;
    config.K = 100;
    config.K_merged = 3;
    config.N_train = 5000;
    config.r_noise = 1000000;
    config.N_test = 50;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.L = myModel->get_L_dimension();
    config.D = myModel->get_D_dimension();
    config.var_obs = vec(config.D, fill::zeros);
    config.functionalModel = std::shared_ptr<FunctionalModel>(myModel);

        config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());
        ExperienceResult result = executeExperience(config);
        std::cout << " sobol : "<< config.r_noise  << std::endl;
        std::cout << result.F_mean * 100 << std::endl;
        std::cout << result.Me_mean * 100 << std::endl;
        std::cout << result.Ce_mean * 100 << std::endl;
        std::cout << result.Yme_mean * 100 << std::endl;
        std::cout << result.Yce_mean * 100 << std::endl;
        std::cout << result.Yb_mean * 100 << std::endl;
        std::cout << result.V1 * 100 << std::endl;
        std::cout << result.V2 * 100 << std::endl;


    config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("latin_cube", config.functionalModel,config.r_noise , 12345).create());
    result = executeExperience(config);
    std::cout << " latin cube : "<< config.r_noise  << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;


    config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("random", config.functionalModel,config.r_noise , 12345).create());
    result = executeExperience(config);
    std::cout << " random : "<< config.r_noise  << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

}

TEST_F(LearningPerformanceTests, HapkeFunctionalTest6){

    double geometries[47*3];
    unsigned i = 0;
    unsigned jump = 0;
    pt::ptree root;
    pt::read_json("../../../test_hapke.json", root);  // Load the json file in this ptree

    //Read geometries
    std::string variables[] = {"eme", "inc", "phi"};
    for(unsigned j=0; j<3; j++){
        i=0;
        jump = 0;
        for (pt::ptree::value_type& v : root.get_child(variables[j]))
        {
            if(jump>=3){
                geometries[i*3+j] = stod(v.second.data());
                i++;
            }
            jump++;
        }
    }

    myModel = std::unique_ptr<Hapke02Model>(new Hapke02Model(geometries, 47, 3,
                                                             std::shared_ptr<HapkeAdapter>(new FourParamsModel(0.5,0.5)),
                                                             30.0));
    ExperienceConfig<DiagCovariance, DiagCovariance> config;
    config.norm = 1;
    config.K = 100;
    config.K_merged = 3;
    config.N_train = 50000;
    config.r_noise = 1000000;
    config.N_test = 5000;
    config.threshold = 0.01;
    config.gllim = gllimDD;
    config.L = myModel->get_L_dimension();
    config.D = myModel->get_D_dimension();
    config.var_obs = vec(config.D, fill::zeros);
    config.functionalModel = std::shared_ptr<FunctionalModel>(myModel);

    config.statModel = std::shared_ptr<StatModel>(DependentGaussianStatModelConfig("sobol", config.functionalModel,config.r_noise , 12345).create());
    ExperienceResult result = executeExperience(config);
    std::cout << " without is : "<< config.r_noise  << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

    config.is = true  ;
    result = executeExperience(config);
    std::cout << " with is : "<< config.r_noise  << std::endl;
    std::cout << result.F_mean * 100 << std::endl;
    std::cout << result.Me_mean * 100 << std::endl;
    std::cout << result.Ce_mean * 100 << std::endl;
    std::cout << result.Yme_mean * 100 << std::endl;
    std::cout << result.Yce_mean * 100 << std::endl;
    std::cout << result.Yb_mean * 100 << std::endl;
    std::cout << result.V1 * 100 << std::endl;
    std::cout << result.V2 * 100 << std::endl;

}

/*
int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    testing::GTEST_FLAG(filter) = "*HapkeFunctionalTest*";
    return RUN_ALL_TESTS();
}*/

