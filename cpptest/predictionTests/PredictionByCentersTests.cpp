//
// Created by reverse-proxy on 5‏/4‏/2020.
//

#include <gtest/gtest.h>
#include "../../src/prediction/Predictor.h"

namespace prediction{
    class PredictionByCentersTests : public ::testing::Test{

    protected:
        void SetUp()override {

        }

    };

    TEST_F(PredictionByCentersTests, generatePermutations){

        ASSERT_TRUE(Predictor::generatePermutations(0).is_empty());

        Mat<unsigned> expectedOnePermutation(1,1,fill::zeros);
        ASSERT_EQ(accu(expectedOnePermutation != Predictor::generatePermutations(1)), 0);

        unsigned N = 3;
        Mat<unsigned> expectedPermutations({
                                         {0,1,2},
                                         {0,2,1},
                                         {1,0,2},
                                         {1,2,0},
                                         {2,0,1},
                                         {2,1,0}
        });
        ASSERT_EQ(accu(expectedPermutations != Predictor::generatePermutations(N)), 0);
    }

    TEST_F(PredictionByCentersTests, regularize){
        mat X_1_est({
                            {1,0},
                            {0,0},
                            {0,2},
                            {0.5,0}
                    });
        mat X_2_est({
                            {0,2},
                            {2,1},
                            {1.5,0},
                            {0,2}
                    });
        mat X_3_est({
                            {3,1},
                            {0,2},
                            {1.5,1},
                            {0,0.5}
                    });
        cube series(4,2,3);
        series.slice(0) = X_1_est;
        series.slice(1) = X_2_est;
        series.slice(2) = X_3_est;

        Mat<unsigned> expectedRegularization({
                                                     {0,1,0},
                                                     {1,0,1}
        });

        //ASSERT_EQ(accu(expectedRegularization != Predictor::regularize(series)), 0);
    }
}