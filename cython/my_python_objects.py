class GLLiMParameters(object):
    def __init__(self):
        self.Pi = 0
        self.C = 0
        self.B = 0
        self.A = 0
        self.Gamma = 0
        self.Sigma = 0