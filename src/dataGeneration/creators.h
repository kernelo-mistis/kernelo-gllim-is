/**
 * @file creators.h
 * @brief This file contains all data generation configuration structs responsible of the creation
 * of various statistic models. Theses structs should be exported to front-end language instead of
 * actual statistical models.
 * @author Sami DJOUADI
 * @version 1.0
 * @date 27/01/2020
 */

#ifndef KERNELO_DATAGENCREATORS_H
#define KERNELO_DATAGENCREATORS_H

#include <string>
#include <memory>
#include <utility>
#include "StatModel.h"
#include "GaussianStatModel.h"
#include "DependentGaussianStatModel.h"

namespace DataGeneration{

    class GaussianStatModelConfig{
    public:
        std::string generatorType;
        std::shared_ptr<FunctionalModel> functionalModel;
        double *covariance{};
        int cov_size{};
        unsigned seed{};
        GaussianStatModelConfig() = default;

        GaussianStatModelConfig(
                std::string generatorType,
                std::shared_ptr<FunctionalModel> functionalModel,
                double *covariance,
                int cov_size,
                unsigned seed){
            this->functionalModel = std::move(functionalModel);
            this->generatorType = std::move(generatorType);
            this->covariance = covariance;
            this->cov_size = cov_size;
            this->seed = seed;
        }

        std::shared_ptr<StatModel> create(){
            return std::shared_ptr<StatModel>(
                    new GaussianStatModel(
                            generatorType,
                            functionalModel,
                            covariance,
                            cov_size,
                            seed)
                    );
        }
    };

    class DependentGaussianStatModelConfig{
    public:
        std::string generatorType;
        std::shared_ptr<FunctionalModel> functionalModel;
        int r{};
        unsigned seed{};
        DependentGaussianStatModelConfig() = default;
        DependentGaussianStatModelConfig(
                std::string generatorType,
                std::shared_ptr<FunctionalModel> functionalModel,
                int r,
                unsigned seed){
            this->functionalModel = std::move(functionalModel);
            this->generatorType = std::move(generatorType);
            this->r = r;
            this->seed = seed;
        }

        std::shared_ptr<StatModel> create(){
            return std::shared_ptr<StatModel>(
                    new DependentGaussianStatModel(
                            generatorType,
                            functionalModel,
                            r,
                            seed)
            );
        }
    };
}

#endif //KERNELO_DATAGENCREATORS_H
