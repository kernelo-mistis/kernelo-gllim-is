User guide
==========

Functional Model
****************
.. autoclass:: kernelo.FunctionalModel
    :members:
.. autoclass:: kernelo.ExternalModelConfig
    :members:
.. autoclass:: kernelo.ShkuratovModelConfig
    :members:
.. autoclass:: kernelo.HapkeModelConfig
    :members:

Data generation
***************
.. autoclass:: kernelo.GaussianStatModelConfig
    :members:

Learning model
**************
.. automodule:: kernelo
    :members: MultInitConfig
    :undoc-members:
    :show-inheritance:
    
Prediction
**********
.. automodule:: kernelo
    :members: PredictionConfig
    :undoc-members:
    :show-inheritance:

Importance sampling
*******************
.. automodule:: kernelo
    :members: GaussianRegularizedPropositionConfig
    :undoc-members:
    :show-inheritance:
