Introduction
============

``Kernelo`` Kernelo is a python module performing efficient inversion of high-dimensional models within a Bayesian framework using GLLiM (Gaussian Locally-Linear Mapping) model approximation.
