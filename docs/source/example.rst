Examples
=============

Installation/Usage:
*******************
As the package has not been published on PyPi yet, it CANNOT be install using pip.
For instructions about how to install, as well as the full documentation of, ``kernelo`` please refer `here <https://gitlab.inria.fr/kernelo-mistis/kernelo-gllim-is>`_

Code example
************
.. literalinclude:: ../../example.py
