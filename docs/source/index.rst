.. Kernelo documentation master file, created by
   sphinx-quickstart on Thu May  4 22:53:00 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Kernelo's documentation!
===================================

``Kernelo`` Kernelo is a python module performing efficient inversion of high-dimensional models within a Bayesian framework using GLLiM (Gaussian Locally-Linear Mapping) model approximation.

Contents:
~~~~~~~~~~

.. toctree::
   :maxdepth: 2

   introduction
   kernelo
   example


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

